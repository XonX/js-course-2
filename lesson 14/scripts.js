function addFourNumbers(a, b, c, d) {
    return a + b + c + d;
}

var sum1 = addFourNumbers(1, 5, 20, 3);
console.log(sum1);
/*
Function above is quite straightforward. It adds  4 numbers together. But what
if the numbers we wanted to add would be contained in an array? In ES5 we have
to use the 'apply' method.
*/

var numArray = [4, 49, 6, 19];
//ES5
var sum2 = addFourNumbers.apply(this, numArray);
console.log(sum2);
/*
ES6, however, has a more convenient way of doing this. And that is the 'spread'
operator: '...'
*/
//ES6
const sum3 = addFourNumbers(...numArray);
console.log(sum3);

/*
Spread can be used to form an array from several different arrays.
*/
const familySmith = ['John', 'Jane', 'Mark'];
const familyMiller = ['Mary', 'Bob', 'Ann'];
const bigFamily = [...familySmith, 'Tim', ...familyMiller];
console.log(bigFamily);

/*
A common use for the spread operator is when we select many different elements
in many different ways and want to apply the same transformation to all of them.
You might also realized that this means that spread also works on NodeLists.
*/
const h = document.querySelector('h1');
const boxes = document.querySelectorAll('.box');
const all = [h, ...boxes];

all.forEach(el => el.style.color = 'purple'); //horrible design choices is how you know I'm a backend developer

/*
Let's say we want to write a function that would accept an unlimited number of
arguments and would perform some operation on all of them. We can do that in ES5
using a special 'arguments' variable, that is accessable from within any
function and contains an argument list of all the arguments passed to the
function. We, of course, want to convert it to an array using the 'slice' trick.
*/
//ES5
// function isLegalAge5() {
//     // console.log(arguments);
//     var args = Array.prototype.slice.call(arguments);
//
//     args.forEach(function (cur) {
//         console.log(2020 - cur >= 18);
//     })
// }
//
// isLegalAge5(1990, 1989, 2019);
// isLegalAge5(1992, 2011, 1987, 1789, 1998);
/*
ES6 provides us with the 'rest' operator. This operator looks exactly like the
spread operator and js interpreter figures out which one to use based on where
the operator is used.
Spread packs all the provided arguments in a neat array that is then usable in
the function.
*/
// //ES6
// function isLegalAge6(...years) {
//     // console.log(years);
//     years.forEach(el => console.log(2020 - el >= 18));
// }
//
// isLegalAge6(1990, 1989, 2019);
// isLegalAge6(1992, 2011, 1987, 1789, 1998);

/*
But what if we want to actually have some additional parameters alongside our
arbitrary list of arguments? Like below, we want to specify the legal age that
we check our birthyears against.
In ES5 we have to use 'slice' to 'slice off' the required number of arguments
from the front of the argument list.
*/
//ES5
function isLegalAge5(limit) {
    console.log(arguments);
    var args = Array.prototype.slice.call(arguments, 1);

    args.forEach(function (cur) {
        console.log(2020 - cur >= limit);
    })
}

isLegalAge5(19, 1990, 1989, 2019);
/*
In ES6 you don't have to change much at all
*/
//ES6
function isLegalAge6(limit, ...years) {
    // console.log(years);
    years.forEach(el => console.log(2020 - el >= limit));
}

isLegalAge6(19, 1990, 1989, 2019);

/*
Now let's look at how we can specify a default value for an argument. In the
example below we assume that a person's last name is Smith and nationality is
American unless otherwise specified.
*/
//ES5
function SmithPerson(firstName, yearOfBirth, lastName, nationality) {
    //should be straightforward enough. If a value is not specify, use the default one
    lastName === undefined ? lastName = 'Smith' : null;
    nationality === undefined ? nationality = 'American' : null;
    this.firstName = firstName;
    this.yearOfBirth = yearOfBirth;
    this.lastName = lastName;
    this.nationality = nationality;
}

var john = new SmithPerson('John', 1990);
console.log(john);
var mark = new SmithPerson('Mark', 1991, 'Miller');
console.log(mark);
/*
In ES6 we can provide default values right in the function definition.
*/
//ES6
function MillerPerson(firstName, yearOfBirth, lastName = 'Miller', nationality = 'British') {
    this.firstName = firstName;
    this.yearOfBirth = yearOfBirth;
    this.lastName = lastName;
    this.nationality = nationality;
}

const mark6 = new MillerPerson('Mark', 1991);
console.log(mark6);