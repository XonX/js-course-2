class Controller {
    constructor(game) {
        this.game = game;
    }

    init() {
        document.getElementById('submit-score').addEventListener('click', () => {
            const scoreInput = document.getElementById('roll-score');
            this.game.roll(scoreInput.value);
            scoreInput.value = '';
        })
    }
}

class Game {
    constructor() {
        this.scores = [];
        this.currentBall = 0;
    }

    roll(rollScore) {
        this.scores[this.currentBall] = rollScore;
        this.currentBall++;
    }

    calculateScore() {
        return this.scores.reduce((score, currentBall, index) => score += currentBall);
    }
}