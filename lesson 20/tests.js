QUnit.module('Controller', (hooks) => {
    let controller, mockGame = {
        submittedScore: 0,
        roll: function (score) {
            this.submittedScore = score;
        }
    };

    hooks.beforeEach(() => {
        mockGame.submittedScore = 0;
        controller = new Controller(mockGame);
        document.getElementById('qunit-fixture').innerHTML = '<input type="number" id="roll-score"><input type="button" id="submit-score" value="Submit">';
        controller.init();
        document.getElementById('roll-score').value = 3;
        clickButtonById('submit-score');
    });

    QUnit.test('score cleared when "Submit" pressed', (assert) => {
        assert.equal(document.getElementById('roll-score').value, '');
    });

    QUnit.test('score submitted to game', (assert) => {
        assert.equal(mockGame.submittedScore, '3')
    });

    function clickButtonById(id) {
        const click = new Event('click');
        document.getElementById(id).dispatchEvent(click);
    }
});

QUnit.module('game', (hooks) => {
    let game;

    hooks.beforeEach(() => {
        game = new Game();
    });

    QUnit.test('failed game', (assert) => {
        rollManyOf(20, 0);

        assert.equal(game.calculateScore(), 0);
    });

    QUnit.test('all ones', (assert) => {
        rollManyOf(20,1);

        assert.equal(game.calculateScore(), 20);
    });

    QUnit.test('all spares', (assert) => {
        rollManyOf(21, 5);

        assert.equal(game.calculateScore(), 150)
    });

   function rollManyOf(balls, score) {
        for (let i = 0; i < balls; i++) {
            game.roll(score);
        }
    }
});