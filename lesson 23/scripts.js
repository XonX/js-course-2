/*
In this lesson we do a little more manipulations with the data we get from the
public API. First, we wrapped the fetch.then chain in a getWeather function and
then added a more interesting console.log then we had before.
*/
function getWeather(woeid) {
    fetch(`https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/${woeid}/`)
        .then(result => {return result.json()})
        .then(data => {
            const today = data.consolidated_weather[0];
            console.log(`Temperatures in ${data.title} stay between ${today.min_temp} and ${today.max_temp}`);
        });
}
/*
getWeather called for London and New York. If you run these functions together
you might discover that sometimes New York gets logged before London, even
though London's WOEID is the first one being looked up. This is because inside
the functions the processes run in Promises. Which means that they run in
separate threads and one can never be truly sure which thread is executed
quicker. This is one of the many things you should be wary when writing
asynchronous code. Not taking this into account might lead to something called
a race condition.
More info here: https://stackoverflow.com/questions/34510/what-is-a-race-condition
*/
getWeather(44418);
getWeather(2459115);
// getWeather(3932049823049823409824309840329840293840294380984320328); - trying invalid WOEID

/*
Now let's rewrite the same function in the async-await fashion just for practice.

Here we also used a trick to deal with errors which can always happen when
dealing with code that may or may not work correctly depending on circumstances.
One such circumstance we have seen above with invalid WOEID being provided.
Another possible problem might be metaweather or cors-anywhere being down. To
handle these errors we use try and catch blocks. Try block surrounds the
potentially problematic code and if an error occures, it is then passed to the
catch block. Here we only log the error, but in real life applications we might
display an error message to the user in a nice way, or try connecting to a
backup service or something else.
*/
async function getWeatherAW(woeid) {
    try {
        const result = await fetch(`https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/${woeid}/`);
        const data = await result.json();
        // console.log(data);
        const tomorrow = data.consolidated_weather[1];
        console.log(`Temperatures in ${data.title} stay between ${tomorrow.min_temp} and ${tomorrow.max_temp}`);
        return data;
    } catch (error) {
        console.log(error);
    }
}

/*
Another thing to keep in mind when working with asynchronous code. Because
getWeatherAW is an asynchronous function, calling it doesn't stop execution in
the global scope and so console.log(dataLondon) in the global scope will be
accessed way before it is set in the .then() and as such will be logged as
undefined
*/
let dataLondon;
getWeatherAW(44418).then(data => {
    dataLondon = data;
    console.log(dataLondon);
});
getWeatherAW(2459115);
console.log(dataLondon);

/*
This concludes the current course module. Next we will put it all together in
coding a large project using all of the modern JS developement practices. Which
means you will have some preparation to do.

HOMETASK
--------------------------------------------------------------------------------
Install node.js on your machine and make sure it works. If you're using work
computer for this course, go to self-service portal and order node.js to be
installed on your PC. For your personal machine you can go to
https://nodejs.org/en/download/ and download it from there. The version I will
be using is closer to the LTS but using latest shouldn't be too big of a problem.
After installing node, you can check that everything is properly working by
opening command line and typing 'node -v'. This should show you the version of
node.js you just installed.
*/