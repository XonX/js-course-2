/*
Starting from this lesson we will be working on a project. This will allow us to
see how everything we've learned so far comes together as well as learn some new
tricks.
The project is a budgeting app.

Before today we were working on small and simple things so we could forgo any
planning and just jump straight to the code. But this project is large enough
for some planning to be in order. Let's compile a todo list.

1) Add event handler to button (controller)
2) Get input values from fields (UI)
3) Add item to internal data structure (Data)
4) Add the item to UI (UI)
5) Calculate budget (Data)
6) Update budget in UI (UI)

All of the above tasks can be clearly divided into 3 types of actions. UI
manipulation, business logic and interaction between the two. This divide
provides us a natural way to divide our code into modules. Dividing code into
logically independant modules is always a good idea, as it facilitates
independant deployability and developability, as well as keeping the codebase
clean and manageble.

Modules:
1) UI module
2) Data module
3) Controller module
*/

//Here's how we create a module
var budgetController = (function () {
/*
First we specify an IEFE, where we put some functions and/or variables. As you
already know, these are completely inaccessable from outside the function, so we
keep the global scope nice and clean and keep all the logic local to our module.
*/
    // var x = 23;
    //
    // var add = function (a) {
    //     return x + a;
    // };
/*
We do however want to interact with our module somehow. To do this, we return an
object that contains variables and functions we want exposed to the outside
world. These are the public functions and variables that we can invoke from
outside the module.
*/
    // return {
    //     publicTest: function (b) {
    //         return add(b); //Thanks to the power of closures, public methods
                              //do have access to module's private entities.
    //     }
    // }


})();


//We didn't write any actual code for the UIController yet, so this is a stub
//for now
var UIController = (function () {
    // code here
})();

/*
The controller module is a little special, because it governs the interactions
between two other modules. As such, we pass the other two modules as arguments
to the IIFE generating it. We could theoretically just use references from the
global scope, but this will tightly couple this module to others and make our
code fragile to file structure and implementation changes. It also reduces code
reusability, as we won't be able to just reuse this controller by passing
different modules to it.
*/
var controller = (function (budgetCtrl, UICtrl) {
/*
Here we see how the controller can use public functions from other modules and
expose public methods of its own.
*/
    // var z = budgetCtrl.publicTest(5);
    //
    // return {
    //     anotherPublic: function () {
    //         console.log(z)
    //     }
    // }
/*
This function shall be called when the "add budget record" button or the Enter
key is pressed. We don't have any logic written yet, but we did mark down a todo
list for it.
*/
    function contrAddItem() {
        //1) Get input data

        //2) add item to budget controller

        //3) add item to the ui

        //4) calculate the budget

        //5) display the budget in the ui
        console.log('Function called');
    }
/*
We add an event listener to our button and pass the contrAddItem() function as a
callback. Note the lack of braces. We want to pass the function as an argument,
not call it.
*/
    document.querySelector('.add__btn').addEventListener('click', contrAddItem);
/*
We also want to call contrAddItem() when "Enter" is pressed. This is how we do
it. Note that we're not adding the listener to any one element, but to the whole
document. This way the listener will be invoked regardless of which element of
the page is active at the moment.
*/
    document.addEventListener('keypress', function (event) {
/*
We do not have a special event for "Enter" so we'll listen to keypress events.
The drawback here is that this event fires every time any key is pressed so we
have to somehow check which key triggered the event. Luckely for us, javascript
allows the callback function to accept the triggering event as an argument and 
we can get the required information from there
*/
        // console.log(event); //Uncomment this to explore the event in the console
/*
We usually use the keyCode parameter to check which key is pressed. Every
character on the keyboard has a numeric keycode associated with it. You can
consult various reference sites to find out keycodes for specific keys, or you
can just uncomment the line above and check keyCodes of events fired when the
key you're curious about is pressed.
KeyCode for "Enter" is 13.
But wait, what is this "which" thing? The thing is, keyCode is a relatively new
part of javascript and as such, older browsers might not support it. So to
support older browsers we should check for this legacy attribute just in case.
Thankfully, keyCode and which values are identical for all keys, so we don't
have to look up two values
*/
        if(event.keyCode === 13 || event.which === 13) {
            // console.log('Enter pressed');
            contrAddItem();
        }
    })
})(budgetController, UIController);