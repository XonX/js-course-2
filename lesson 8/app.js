/*
In this lesson we implement item deletion. To understand this lesson you need to
understand the concept of event bubbling. When an event gets fired on an element
it then gets fired on the element that is the parent of the original element.
Then in turn on its parent etc. So it "bubbles up".

For example, if we have an html document like this:

<html>
	<body>
		<button title="button"/>
	</body>
</html>

and we click the button, the 'click' event will first get fired on the 'button'
element, then on the 'body' element and finally, on 'html' element. Using this,
we can attach a single event listener to control a group of elements by attaching
it to a parent element that unites them.
*/
var budgetController = (function () {
    var Expense = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1
    };

    var calculateTotal = function (type) {
        var sum = 0;
        data.allItems[type].forEach(function (cur) {
            sum += cur.value;
        });
        data.totals[type] = sum;
    };

    return {
        addItem: function (type, des, val) {
            var newItem, ID;

            if (data.allItems[type].length > 0) {
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            } else {
                ID = 0;
            }

            if (type === 'exp') {
                newItem = new Expense(ID, des, val);
            } else if (type === 'inc') {
                newItem = new Income(ID, des, val);
            }

            data.allItems[type].push(newItem);

            return newItem;
        },

        testing: function () {
            console.log(data);
        },

        calculateBudget: function () {
            calculateTotal('exp');
            calculateTotal('inc');
            data.budget = data.totals.inc - data.totals.exp;
            if (data.totals.inc > 0) {
                data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
            } else {
                data.percentage = -1;
            }
        },
/*
This function is used to delete the relevant item from our data structure.
First we need to get an index of an item with a specific id and then delete the
item with said index from the corresponding array which we select using 'type'
*/
        deleteItem: function (type, id) {
            var ids, index;

            //why we can't jus use id as index
            //inc: [0 1 2]
            //delete item with id 1 - inc[1]
            //inc: [0 2]
            //delete item with id 2 - inc[2] - error. Index out of bounds

/*
We use the "map" function to get an array of ids. "Map" accepts a callback as an
argument and then goes through the array element by element applying the callback
every time. It then collects all of the results of the callback function calls
and returns them as a new array.
*/
            ids = data.allItems[type].map(function (current) {
                return current.id;
            });

            //finding the index of a specific id
            index = ids.indexOf(id);

            if (index !== -1) {
/*
"Splice" is a function that can separate an array in two. First argument is the
index starting from which we want to start the splicing. The second element
is how many elements do we want to splice. The return value of this function is
the spliced elements while the original array is modified so that these elements
are removed.

For example if we have an array
var arr = [4, 3, 5, 19, 7, 8]
and call arr.splice(2, 3), the return value will be [5, 19, 7] and arr will become
equal to [4, 3, 8].

In our case we just want to delete an element, so we don't save the return value
anywhere.
*/
                data.allItems[type].splice(index, 1);
            }
        },

        getBudget: function () {
            return {
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            }
        }
    }
})();

var UIController = (function () {
    var DOMstrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: '.income__list',
        expenseContainer: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        container: '.container' //new thing added, because we need to reference the container element
    };

    return {
        getInput: function () {
            return {
                type: document.querySelector(DOMstrings.inputType).value,
                description: document.querySelector(DOMstrings.inputDescription).value,
                value: parseFloat(document.querySelector(DOMstrings.inputValue).value)
            }
        },

        getDOMstrings: function () {
            return DOMstrings;
        },

        addListItem: function (obj, type) {
            var html, newHtml, element;
            //1) create HTML string with placeholder text
            if (type === 'inc') {
                html = '<div class="item clearfix" id="inc-%id%">\n' +
//NB! we changed the ids from "income-%id%" to "inc-%id%" and done the same with
//expenses. This is to easily use parts of the id to reference relevant parts
//of our data structure
                    '  <div class="item__description">%description%</div>\n' +
                    '    <div class="right clearfix">\n' +
                    '      <div class="item__value">+ %value%</div>\n' +
                    '      <div class="item__delete">\n' +
                    '        <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\n' +
                    '      </div>\n' +
                    '    </div>\n' +
                    '  </div>';
                element = DOMstrings.incomeContainer
            } else if (type === 'exp') {
                html = '<div class="item clearfix" id="exp-%id%">\n' +
                    '    <div class="item__description">%description%</div>\n' +
                    '    <div class="right clearfix">\n' +
                    '        <div class="item__value">- %value%</div>\n' +
                    '        <div class="item__percentage">21%</div>\n' +
                    '        <div class="item__delete">\n' +
                    '            <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\n' +
                    '        </div>\n' +
                    '    </div>\n' +
                    '</div>';
                element = DOMstrings.expenseContainer;
            }
            //2) replace the placeholder with actual data
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', obj.value);
            //3) insert resulting HTML into DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },

        clearFields: function () {
            var fields, fieldsArr;
            fields = document.querySelectorAll(DOMstrings.inputDescription + ', ' + DOMstrings.inputValue);
            fieldsArr = Array.prototype.slice.call(fields);
            fieldsArr.forEach(function (c, index) {
                //console.log(index + ': ' + c.value);
                c.value = '';
            });

            fieldsArr[0].focus();
        },

/*
This function deletes the income or expense element from the ui. Element objects
don't have "delete" or "remove" methods for themselves. You can only remove a
child of a specific element. That is why after selecting an element we want to
delete we first save it and then call "removeChild" on its parent.
*/
        deleteListItem: function (selectorId) {
            //document.getElementById(selectorId).parentNode.removeChild(document.getElementById(selectorId));
            var el = document.getElementById(selectorId);
            el.parentNode.removeChild(el);
        },

        displayBudget: function (obj) {
            document.querySelector(DOMstrings.budgetLabel).textContent = obj.budget;
            document.querySelector(DOMstrings.incomeLabel).textContent = obj.totalInc;
            document.querySelector(DOMstrings.expensesLabel).textContent = obj.totalExp;
            if (obj.percentage > 0) {
                document.querySelector(DOMstrings.percentageLabel).textContent = obj.percentage + '%';
            } else {
                document.querySelector(DOMstrings.percentageLabel).textContent = '---';
            }
        }
    }
})();

var controller = (function (budgetCtrl, UICtrl) {
    var setupEventListeners = function () {
        var DOM = UICtrl.getDOMstrings();

        document.querySelector(DOM.inputBtn).addEventListener('click', contrAddItem);
        document.addEventListener('keypress', function (event) {
            if (event.keyCode === 13 || event.which === 13) {
                contrAddItem();
            }
        });
/*
We are going to use event bubbling to capture 'click' events on all of our delete
buttons. This will, of course, capture all of the 'click' events inside the
container, but we will code a check to separate relevant events from the
irrelevant inside ctrlDeleteItem function.
*/
        document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem);
    };

    var updateBudget = function () {
        var budget;
       //1. calculate the budget
        budgetCtrl.calculateBudget();
       //2. return the budget
        budget = budgetCtrl.getBudget();
        // console.log(budget);
       //3. display the budget in the ui
        UICtrl.displayBudget(budget);
    };


    function contrAddItem() {
        var input, newItem;
        //1) Get input data
        input = UICtrl.getInput();

        if(input.description !== '' && !isNaN(input.value) && input.value > 0) {
            //2) add item to budget controller
            newItem = budgetCtrl.addItem(input.type, input.description, input.value);

            //3) add item to the ui
            UICtrl.addListItem(newItem, input.type);
            // console.log(newItem);
            //3.5) clear input and set focus
            UICtrl.clearFields();

            updateBudget();
        }
    }

    function ctrlDeleteItem(event) {
        var itemID, splitID, type, ID;
/*
Event object has a 'target' property that always contains the original element
that the event got fired on. This way we can find the relevant element regardless
of where we actually caught the event in the bubbling chain. Of course, said
element is just an icon for the button and not the whole element we want to
select or delete, so we need to use the "parentNode" property until we get the
root element of the income/expense entity.
We then select the id of said element.
*/
        itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
/*
Elements we want to be deletable are the only elements in the documents that
have ID-s, so it is safe to assume, that if the element at the end of "parentNode"
chain didn't have an id, the target element was not a delete button. This is how
we filter out the irrelevant 'click' events.

It's not the most robust of checks and heavily relies on ui structure being
unchanged, but it is good enough for our simple app
*/
        if (itemID) {
/*
Here we use the 'split' function to separate the id into the type and the id.
Called on a string, 'split' returns an array of strings that are split on the
character sequence provided as argument.

For example
'aa-bb-cc'.split('-') would return ['aa', 'bb', 'cc']

If you were paying very close attention to the topic of types in javascript, you
might be a little surprised that we can call a function on a 'string' type
variable. 'Sting' is a primitive, not an object and as such, shouldn't have any
callable funtions. This is also a part of javascripts magic type conversion.
Seeing a function being called on a string, it wraps the primitive in a 'String'
object and calls the requested method on that.
*/
            splitID = itemID.split('-');

            type = splitID[0];
            ID = parseInt(splitID[1]);

            //1. delete the item from data structure
            budgetCtrl.deleteItem(type, ID);
            //2. delete the item from the UI
            UICtrl.deleteListItem(itemID);
            //3. update and show the new budget
            updateBudget();
            //Note that we didn't have to write any new code for the third item
            //as the 'updateBudget' function we wrote before does exactly what
            //we need and so we can reuse it.
        }
    }

    return {
        init: function () {
            setupEventListeners();
            UICtrl.displayBudget({budget: 0, totalExp: 0, totalInc: 0, percentage: -1});
            console.log('Application started');
        }
    }

})(budgetController, UIController);

controller.init();