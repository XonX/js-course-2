/*
So far we have covered everything there is to know about the ES5 version of JS.
However, the language has developed quite a lot since the days of ES5 and now it
is time to move on to the newer version of the language and explore what has
changed.

First of all, we have 2 new ways to declare a variable, let and const
*/

let ba = 'ba';
const cd = 'cd';
//cd = 'f'; will give an error
//const f; also error. Consts should be defined when declared

/*
First and most obvious change is that we now have a variable, whose value we can
not change. These are 'const' variables. These are perfect for storing constants
and other values you don't want changed.
'let' variables will let you change their values. However they still behave
differently than your regular 'var' variables.

'var' variables are function scoped, meaning that they are visible in the scope
of the function they are declared. 'let' and 'const' are block scoped. Look at
the example below, to see what that means.
*/

function displayOrNot5(display) {
    console.log(a); //undefined. Variable accessable before declaration. See previous lectures if you forgot why
    if(display) {
        var a = 20;
        var b = '40';
    }
    console.log(a, b); //works ok. Variables declared within the function
}

displayOrNot5(true);


function displayOrNot6(display) {
    // console.log(a); error. 'let' variable can't be accessed before it's defined.
    if(display) {
        let a = 20;
        const b = '40';
        console.log(a, b);
    }
    // console.log(a, b); error. Variables called outside the block they are defined in
}

displayOrNot6(true);

/*
Notice the last commented out 'console.log'. 'a' and 'b' can not be called there
because they are defined within an 'if' block and aren't accessible outside of it.
This is what 'block scoped' means.

All of the aforementioned properties give you as a programmer more control over
the variable visibility and helps make code cleaner. That's why, if you're writing
code for a modern browser it is advised to avoid using 'var' whenever possible.

Let's look at a more 'real-life' example of how block scoping can be useful.
*/

let i = 23;

for (let i = 0; i < 5; i++) {
    console.log(i);
}

console.log(i); //23. Brackets in front of the 'for' are also considered part of the 'for' block


var j = 23;

for (var j = 0; j < 5; j++) {
    console.log(i);
}

console.log(i); //5. 'j' is function scoped so it gets overwritten by the 'for'

/*
So far we have seen 'if' and 'for' blocks used to scope our new variable types.
But you don't have to use them if you want to declare a block. All you really
need to do is to use '{}' like below.
*/
{
    const abc = 'abc';
    let ab = 'ab';
    var c = 'c';

    console.log(abc, ab);
}

//console.log(abc, ab) - error. Variables invisible outside the block
console.log(c); //'var' doesn't care about blocks

/*
If you're paying attention, you might notice that this is similar to how an IIFE
behaves (below as reference). And indeed, it is. As long as you don't use any
vars, you can avoid the bulky IIFE declaration and achieve the same result by
just using a block. Another reason to avoid vars.
*/

(function() {
    var private = 'private';
})();

/*
ES6 also adds some new ways to work with strings. Compare for example ES5 and ES6
ways of adding variable values to a string. Notice that ES6 string has backticks
'`' instead of single quotes. That is how you tell js that you intend to use
string literals '${}' in the string. Notice also, that you can use simple
expressions, such as function calls as string literals.
*/

let firstName = 'John';
let lastName = 'Smith';
const yearOfBirth = 1990;
function calcAge(year) {
    return 2020 - year;
}

//ES5

console.log('Hello, I am ' + firstName + ' ' + lastName + '. I was born in '
    + yearOfBirth + ' and I am ' + calcAge(yearOfBirth) + ' years old.');

//ES6

let greeting = `Hello, I am ${firstName} ${lastName}. I am ${calcAge(yearOfBirth)} years old.`;

console.log(greeting);

//below are some new string functions ES6 has to offer
//I think they're pretty self explanatory, so I won't write about them in-depth
console.log(greeting.startsWith('Hel'));
console.log(greeting.startsWith('hel'));

console.log(greeting.endsWith('Hel'));
console.log(greeting.endsWith('ld.'));

console.log(greeting.includes('Smith'));

console.log('bark '.repeat(5));
