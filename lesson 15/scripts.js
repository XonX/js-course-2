/*
Today we have learned of several new ES6 features. First of them is map. Map is
a data structure where each value is assigned to a coressponding key in what is
called key-value pairs. You might feel like that is very similar to how an object
is organized in js and you'd be right. Why then have a new entity if it works
exactly like the old one?
The reason is that the Map object has several built-in methods for operating data
contained within, that make it a much more convinient choice then an object for
some operations.
*/

const question = new Map();
question.set('question', 'What is the name of Earth\'s natural satellite');
question.set(1, 'Io'); //keys can be of different data types
question.set(2, 'Moon');
question.set(3, 'Phobos');
question.set(4, 'Deimos');
question.set('correct', 2);
question.set(true, 'Correct!'); //even boolean is valid
question.set(false, 'Wrong. Are you even from Earth?');

//This is how we get a value from the map, using a correspoinding key
console.log(question.get('question'));

console.log(question);

if (question.has(4)) { //we can check if a map has a certain key
    console.log(question.get(4));
}
question.delete(4); //we can delete a key-value pair using a key reference
question.delete(4); //if we call 'delete' on a value that isn't present in a map, nothing happens
if (question.has(4)) {
    console.log('You shouldn\'t be here');
}

console.log(question);

/*
Map also has a forEach implememntation, which makes traversing it a much simpler
task then it would be to traverse an object. Values and corresponding keys are
provided to the callback function
*/
question.forEach((value, key) => console.log(`This is ${key} and it's set to ${value}`));

/*
We could also use the for-of loop to achieve the same thing, however since
individual elements of a map are key-value pairs, each iteration of the loop will
deal with such a pair, and not individual values
*/
for (let pair of question.entries()) {
    console.log(pair);
}

console.log('-------------------------------------');
console.log(question.get('question'));
/*
We can still get individual keys and values, however, if we use destructing
*/
for (let [key, value] of question.entries()) {
    if (typeof(key) === 'number') {
        console.log(`${key}: ${value}`);
    }
}

const answer = parseInt(prompt('Write the correct answer'));

/*
Below you can witness neat things that are achivable because keys can be boolean.
First, we got an answer for the user (converting it to a number), then we use
=== operator to check if it's the same as the correct answer. This then evaluates
to either true or false, depending on the answer. And this evaluated value is
then used to get the correct response message from the map.
*/
console.log(question.get(answer === question.get('correct')));

/*
You can assign values to keys already present in the map. They will just
overwrite the old values
*/
question.set(true, 'How did I get here?');
console.log(question);

//don't forget to wash your hands
question.clear();
console.log(question);

/*
Next, we will explore another new feature of ES6 that is classes. They make
creating new object types much easier and more streamlined. But first, let's
remind ourselves how creating an object type and attaching a method to it works
in ES5
*/
//ES5
var Person5 = function (name, yearOfBirth, job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
};

Person5.prototype.calculateAge = function () {
    console.log(new Date().getFullYear() - this.yearOfBirth);
};

/*
Not too complicated, if not too pretty. But now look how this looks in ES6. Much
better and logically structured.
*/
//ES6

class Person6 {
/*
Constructor sets the initial state of the object. It's what is called, when a
new instance of an object is created.
*/
    constructor(name, yearOfBirth, job) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.job = job;
    }
/*
Notice that we don't need any separators or function statements to add a
function to our class
*/
    calculateAge() {
        console.log(new Date().getFullYear() - this.yearOfBirth);
    }
/*
Here's another new thing. Usually, to call object methods we need to have an
object instance to call them on. Like we do with 'john.calculateAge()' below.
With static methods, however, you don't need an instance. In fact, you can't
call them on an object instance. You need to use the class reference, like we do
with 'Person6.greeting()'.
Static methods are usually used to define helper or utility functions that are
logically connected to the object type.
A good example is 'Array.isArray()' function. If we want to check if a variable
'arr' is an array, we can't call 'arr.isArray()', since the whole point is that
we don't know the type and so we don't know if 'arr' actually has such a method.
So, to be useful, this method can't be tied to an object instance. It is however
strongly logically linked to the concept of an array. And so it is defined as a
static method of the Array class.
*/
    static greeting() {
        console.log('Hello there!');
    }
}

const john = new Person6('John', 1990, 'journalist');
john.calculateAge();

Person6.greeting();

/*
As cool as classes are in ES6, where they really shine is 'inheritence', or
creation of subclasses.
Subclass is when a class is derived from (is a 'child' of) another class (named
'superclass' or 'parent' class). We did not do much work with subclasses before
and for a good reason. In ES5 inheritence is quite clunky and difficult to wrap
your head around. Let's see, what we need to do, to create a subclass of a
'Person5' class.
*/
//ES5
var Athlete5 = function (name, yearOfBirth, job, olympicGames, medals) {
/*
This line is the first step to creating a subclass. Let's try to wrap our heads
around it and figure out how it works.
First, you have to remember, that object type definitions in js are actually
functions. And when we create a new object instance using the 'new' keyword, what
actually happens is
1. an empty object gets created
2. the referenced function is called with 'this' set to refer to the newly
created object
3. object is then returned as a result
Second, you need to remember that 'call' is used to call a function with a
predefined value of 'this'. So what actually happens when we call new Athlete5
is that the newly created empty object gets passed to 'Person5' as a value of
'this' as well.

Remember that joke about javascript 'this is bs'? Yeah. It never gets old
because it never stops being true.
*/
    Person5.call(this, name, yearOfBirth, job);
    this.olympicGames = olympicGames;
    this.medals = medals;
};

/*
Second step of establishing inheritence. We tie Person5's function to Athlete5's
prototype property.
*/
Athlete5.prototype = Object.create(Person5.prototype);

/*
And now finally we can add some Athlete5-specific functions. It is important to
do this after we did the 'Object.create' (static method, by the way) thing,
because it overwrites the 'prototype' property and so all the functions
specified before that would get erased.
*/
Athlete5.prototype.wonMedal = function () {
    this.medals++
};

var tom = new Athlete5('Tom', 1990, 'runner', 3, 2);
console.log(tom);
tom.wonMedal();
console.log(tom);

/*
I hope you've spent a lot of time thinking long and hard trying to figure out
the thing above. Because you don't need any of it :)
Here's how you achieve the same result in ES6. You use 'extends' to specify the
superclass of your class, you call 'super' to run the superclass' constructor
in your constructor (you actually have to do it. Js will throw an error
otherwise) and you're good to go.
*/
//ES6
class Athlete6 extends Person6 {
    constructor(name, yearOfBirth, job, olympicGames, medals) {
        super(name, yearOfBirth, job);
        this.olympicGames = olympicGames;
        this.medals = medals;
    }

    wonMedal = function () {
        this.medals++
    }
}

var sam = new Athlete6('Sam', 1989, 'swimmer', 4, 6);
sam.wonMedal();
console.log(sam);

/*
You have learned a lot of new ES6 stuff by now. So it is time to practice it.
Here's a HOMETASK.

Imagine that you are an assistant in a government of a small town. The town has
1. Parks
2. Streets

Since the town is small, it only has 3 parks and 4 streets. But more can be
added in the future.
Both parks and streets have a name and a year when they were built.

You are asked to generate a report containing various info regarding parks and
streets.

Generate report, print it to console.
1) Tree density for each park (# of trees / park area)
2) Average age of parks (sum of ages / # parks)
3) Name(s) of a park(s) that have more thAn 1k trees
4) Total and average length of the town's streets
5) Size classification of each street (tiny/small/normal/big/huge)

Use ES6 features you have learned: classes, subclasses, template strings,
default params, maps, arrow functions etc

Usually, hometasks contain step-by-step guids on how to do them, but this time
I would like you to try and figure it out yourselves. Indeed, in the real world
of programming your customer won't tell you how to code a feature step-by-step ;)
*/
