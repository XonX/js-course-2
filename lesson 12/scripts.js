/*
Today we will learn about arrow functions. These allow to write one-use functions
and callbacks in a more consice manner as well as mitigate some of the js'
'particularities'.

Let's imagine that we have an array of years and we wnat to calculate ages based
on said years. Here's how we would do it in ES5:
*/
const years = [1999, 1989, 2000, 1985];

//ES5
var ages5 = years.map(function (el) {
    return 2020 - el;
});
console.log(ages5);

/*
And here's what ES6 allows us to do.
This is the simplest form of an arrow function that can be formalized as
{argument} => {return statement} and works almost identical to writing

function ({argument}) {
    return {return statement}
}
*/
//ES6
let ages6 = years.map(el => 2020 - el);
console.log(ages6);

/*
That's all well and good, but what if we want to have an arrow function with two
arguments? If you remember, the way 'map' works allows us to get not noly the
values of elements, but also their indexes within the array.

To have an arrow funcion with more then one argument, we just have to surround
them with parentesis ()
*/
ages6 = years.map((el, index) => `Age element ${index + 1}: ${2020 - el}`);
console.log(ages6);

/*
We are, of course, not limited to just the return statement either. We can make
a proper function this way. With variables and statements and everything.
We do have to explicitly write out the return statement in this case though.
*/
ages6 = years.map((el, index) => {
    const currentYear = new Date().getFullYear();
    const age = currentYear - el;
    return `Age element ${index + 1}: ${age}`;
});
console.log(ages6);

/*
Remember how I said that arrow functions and regular function definitions are
almost identical? Well, the 'almost' can be quite important in some cases.
The main difference between arrow funcions and regular functions is something
called 'the lexical "this" keyword'.
In regular functions 'this' either refers to the global object or the objet that
the function is attached to. Arrow functions, however, inherit the value of 'this'
from the surrounding code. It can be hard to understand just by explanations so
let's look at an example.
*/

//ES5
var box5 = {
    color: 'green',
    position: 1,
    clickMe: function () {
        var self = this;
/*
The line above is very important to understanding the example. Try commenting it
out and replacing all references to 'self' with 'this'. Notice how value of 'this'
changes in the function given as a callback to 'addEventListener'? That is because
it gets attached to a DOM element and thus its value of 'this' is defined by that
and the fact that in the surrounding code 'this' means something entirely
different means nothing.

The pattern of using 'var self' or something similar to make callbacks refer to
a correct object is quite common in ES5 era javascript.
*/
        console.log(this);
        document.querySelector('.green').addEventListener('click', function () {
            var string = 'This is box number ' + self.position + ' and it is ' + self.color;
            console.log(this);
            console.log(self);
            alert(string);
        })
    }
};
// box5.clickMe();

/*
Let's see how ES6 and lexical 'this' keyword help us mitigate this issue.

Notice how the value of 'this' remains the same both outside the callback and
inside? That is what 'lexical "this" keyword' means. The value of 'this' within
the arrow function is the same as in the surrounding code. And thus we can use it
in the callback without having to using the 'self' pattern.
*/
const box6 = {
    color: 'green',
    position: 1,
    clickMe: function () {
        console.log(this);
        document.querySelector('.green').addEventListener('click', () => {
            const string = 'This is box number ' + this.position + ' and it is ' + this.color;
            console.log(this);
            alert(string);
        })
    }
};
// box6.clickMe();

//a reminder on how method borrowing using 'bind' function works.
let box7 = {position:2, color:'red'};
let borrowed = box6.clickMe.bind(box7);
borrowed();

function Person(name) {
    this.name = name;
}

/*
Here's another example on how you can overcome 'this' weirdness in ES5.
We use 'bind' to create a copy of a function we've defined and explicitly set the
value of 'this' to the object it's defined in.
*/
//ES5
Person.prototype.myFriends5 = function (friends) {
    var arr = friends.map(function (el) {
        return this.name + ' is friends with ' + el;
    }.bind(this));

    console.log(arr);
};

var friends = ['Jake', 'Bob', 'Sam'];
new Person('John').myFriends5(friends);

//ES6...Hometask :)
//Write a 'myFriends6' method for the Person object, that does the same thing
//as 'myFriends5', but uses the cool new ES6 features you have learned.