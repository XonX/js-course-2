/*
In this lesson we revisit the concept of promises and introduce the async and
await keywords. If you don't remember the topic of promices, check back to
lesson 16. The code below is copied and pasted from that lesson.
*/

const getIDs = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve([876, 395, 558, 787]);
    }, 1500);
});

const getRecipe = recipeID => {
    return new Promise(((resolve, reject) => {
        setTimeout(ID => {
            const recipe = {
                title: 'Dumplings',
                publisher: 'Juri'
            };
            resolve(recipe);
        }, 1500, recipeID)
    }))
};

const getRelated = publisher => {
    return new Promise(((resolve, reject) => {
        setTimeout(pub => {
            const recipe = {
                title: 'Pizza',
                publisher: 'Juri'
            };
            resolve(`Another recipe published by ${publisher}: ${recipe.title}`);
        }, 1500, publisher);
    }))
};


// getIDs.then(IDs => {
//     console.log(IDs);
//     return getRecipe(IDs[1]);
// }).then(recipe => {
//     console.log(`${recipe.title}`);
//     return getRelated(recipe.publisher);
// }).then(anotherRecipe => {
//     console.log(anotherRecipe);
// }).catch(error => console.log(error));
/*
Commented out code above is how we used to work with promices. It might seem
a little clunky and counterintuitive. Async and await provide a less confusing
alternative.

async keyword before a function definition marks the function as asynchronous.
This means that, unlike a regular function, when such a function is invoked, the
execution of the programm doesn't stop to wait for the function to finish
executing. Instead, the function is executed on a separate thread. Effectively
"runs in background". In the example below
console.log('end')
will execute before getRecipesAW prints anything, because all its outputs are
waiting for a timeout.

await is the opposite of that. Await tells our programm to halt execution until
a provided promise is resolved. It then returns a value provided to the
resolve() function in the promise. Using await in an async function, we can
recreate the chained promises behaivour from lesson 16, but in a way that closer
resembles the traditional program flow.
*/

async function getRecipesAW () {
    // setTimeout(() => {
    //     console.log('async timeout');
    // }, 1500);
    const ids = await getIDs;
    console.log(ids);
    const recipe = await getRecipe(ids[2]);
    console.log(`${recipe.title}`);
    const anotherRecipe = await getRelated(recipe.publisher);
    // console.log(anotherRecipe);
    return anotherRecipe;
}

/*
But what if we want for our function to return a value and then do something
with it? If we try doing it like we do with a regular function, we will discover
that it doesn't return the value we expected, but instead it returns a promice.

If you think about it, it makes sense, because in the moment that we assign the
function's return value to a variable, the function itself might still be
executing in its separate thread (and in our case, it does). So an async
function always returns a promice and we should use the .then() method to do
anything with it.
*/
// const recipe = getRecipesAW();
// console.log(recipe);

getRecipesAW().then(result => console.log(result));

console.log('end');

/*
Next we delved into the wonderful world of AJAX and APIs. APIs allow our app to
communicate to other apps and services and get info from them asynchroniously.

You might have an internal API to communicate to a backend server, but there are
also publicly available APIs. One example of such an API is metaweather.com.
Let's try and fetch fome info from it using the fetch() function.
*/
//AJAX - Asynchronous JavaScript And XML
//API - Application Programming Interface

fetch('https://www.metaweather.com/api/location/44418/').then(result => console.log(result)).catch(error => console.log(error));
/*
The code above produces an error. This is because the API doesn't implement
Access-Control-Allow-Origin policy. This policy allows Cross-Origin Resource
Sharing.

Basically, the browser knows where the page it loads is "coming from" (what's
it's origin) and by default it restricts loading any resources that don't come
from the same origin unless the other server has Access-Control-Allow-Origin
policy configured. This is done to make hackers' lives harder and is generally
a good idea.

It does however mean that we can't just grab metaweather API and use it in our
javascript. How would we bypass this inconvinience?

Usually, we would have a backend server that would make the request to the
remote API. Since we control the server-side code we can make sure it only
connects to our trusted APIs and can even do some filtering and transformation
of data so that the frontend recieves only data that is actually relevant.

So, the frontend code makes a request to backend, backend makes a request to
remote API and returns the response the frontend. Since backend's response comes
from the same origin as the original page (our backend server), browser allows
the response to be loaded.

Right now however, we don't have a backend server to do our requests for us.
Luckely, some nice people have created an app that adds a correct
Access-Control-Allow-Origin policy to any public API request. So the code below
will work.
*/

//Access-Control-Allow-Origin allows to bypass same-origin restrictions. Need to be implemented on API side.

fetch('https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/44418/')
    .then(result => {return result.json()})
    .then(data => console.log(data));

/*
WARNING!

Using cors-anywhere or similar services is perfectly fine for purposes of
experimentation, learning and the like. DO NOT use it in live production code,
however.
*/