QUnit.test('Clear input field on submit', (assert) => {
    document.getElementById('roll-score').value = 3;
    const click = new Event('click');

    document.getElementById('submit-score').dispatchEvent(click);

    assert.equal(document.getElementById('roll-score').value, '');
})