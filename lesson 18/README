As those of you who have seen the lesson know, this lesson has been a subject to
insane levels of demo effect. So instead of the promised video, you get this writeup
of the theory presented and working version of the source code.

The topic of the lesson is TDD - Test Driven Developement. It is a way of developing
software that is very popular among proponents of Clean Code. I try to do it as much
as possible and recommend that everybody else do the same. The crux of the method is
that you're not allowed to write any production code that doesn't fix a failing test.
Which means that to write anything, you must first write a test for it. What that
guarantees is that all the code written in this way is covered in automatic tests.
And, therefore, can be tested at a press of a button.

TDD is not exclusive to javascript. Pretty much every widely used programming language
has at least one unit testing framework and as such can be worked with in a test-first
manner. If you learn the underlying principles of TDD, you can apply it anywhere and
thus this makes this and the following lesson one of the most important ones in the
whole course.

To demonstrate TDD in action, I have chosen a classic coding kata called "bowling game".
Kata is a word that comes from Japan, from the world of martial arts. In martial arts
kata is a series of movements and poses that pupils practice to commit them to muscle
memory. In coding katas are known well defined problems that you might solve and practice
not to write a working programm, but to practice a language, or a framework, or a method.
The "bowling game" kata requires us to write a programm that would accept as inputs
scores for individual ball rolls and would output a score for the whole game in the
end.

Rules of scoring are as follows:
1. Player gets 2 tries to knock down 10 pins. This is called a "frame".
2. If a player doesn't knock down all 10 pins in a frame, that is called an open frame
and the score for it == number of knocked down pins
3. If a player knocks down all pins in 2 throws it is called a spare and the score for
this frame == 10 + score for the next roll
4. If a player knocks down all the pins on his first ball of frame, it is called a strike.
The score for such a frame == 10 + score for next 2 rolls
5. If a player gets a spare or a strike on his last frame 1 or 2 bonus balls are thrown
to determine the final score for the frame.

TDD is a cycle consisting of 3 steps. Red, green and refactor.
In red phase, your task is to write the most simple and basic failing test. And not
compiling is failing. In javascript, we don't really have a notion of "not compiling" but
we can easily adjust this to mean that runtime errors (such as calling undifined methods)
count as failing.
In the green phase you need to make the tests pass. Write as little code as possible to do
it.
In the refactor phase you look at the code and see if something needs refactoring. can
you put something in a method, is there duplicate code, is something in need of a rename?
Then you go back to the red phase and the cycle begins anew.

In this lesson I've only managed to do one such cycle and the result you can see in the
included sources. Next lesson I'll hopefully be able to jump right in and show you
proper TDD without the demo effect :)

Things that you should definitely understand going into the next lesson is the composition
of the tests.html file.
First thing to notice are
<link rel="stylesheet" href="https://code.jquery.com/qunit/qunit-2.9.2.css">
in the head tag and
<script src="https://code.jquery.com/qunit/qunit-2.9.2.js"></script>
in the body.
These lines import the required framework files from the remote server and enable the magic
to work.

You might also notice these 2 divs:
<div id="qunit"></div>
<div id="qunit-fixture"></div>
They are required by QUnit (the unit testing framework of our choice). First one is used
by QUnit to display the test results in a pretty report.
The secons one is a little special. It is where we should put html required for our code to
run. For example, if we want to test an function that adds an event handler to some button
we need to have the required button in the DOM of the testing page, otherwise the code
will fail.

Be warned, however! The contents of this div get scrapped and regenerated every time and
new test is run. This is what got me on the lesson. I did put the button there, but when
the test started, the button that had the event listener attached got deleted and a new,
clean button got generated in its stead. You can work around it by putting the required
elements outside of the div and before all the scripts like I did in this example, but this
will inevitably lead to problems if you leave it like that.

Which is a good thing.

The beauty of TDD is that it's much harder to write bad code, because it is much harder
to test bad code. So issues with tests will signal to you early on that what you are
doing is way off the mark. As it did for me, because it's not a good practice to do
things in global scope with JS. You want stuff hidden in objects and controllers. And
QUnit, by being uncooperative otherwise, forces you to do so very early on. So this
is what we're going to do next time.

Stay tuned ;)