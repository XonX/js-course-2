var CURRENT_YEAR = 2019;

function drinkingAgeMessage(age) {
    var a = 'Is of drinking age';
    var b = 'Is not of drinking age';
    return function (dateOfBirth) {
        if(CURRENT_YEAR - dateOfBirth >= age) {
            console.log(a);
        }
        else {
            console.log(b);
        }
    }
}

var drinkingAgeEurope = drinkingAgeMessage(18);
var drinkingAgeUS = drinkingAgeMessage(21);

drinkingAgeEurope(1989);
drinkingAgeEurope(2001);
drinkingAgeUS(1989);
drinkingAgeUS(2001);

/*
The above excersise demonstarates the concept of closures. Closures refer to the
fact that in js the inner function always has access to its parent function's
parameters and variables. Even if the outer function has finished executing.
In the example above drinkingAgeMessage() is executed only twice, when
drinkingAgeEurope and drinkingAgeUs are created. However, each invocation of
these "child" functions have access to outer variables 'a' and 'b' and the value
of parameter 'age' they were generated with.

This is made possible by the fact that when an execution context gets destroyed,
the variable object associated to it is kept in memory, so every time a child
funcion is called, it gets access to said variable object via scope chain.
If you're having trouble remembering details about execution contexts and 
varable objets you can refresh your memory by rereading this lecture:
https://bitbucket.org/XonX/js-course/src/master/lesson6/scripts.js

Now it's time to move on to something even cooler.
*/

var john = {
    name: 'John',
    age: 20,
    occupation: 'developer',
    greeting: function (style, timeOfDay) {
        if(style === 'formal') {
            console.log('Good ' + timeOfDay + ', ladies and gentlemen! My ' + 
                'name is ' + this.name + ', I am ' + this.age + 'years old ' + 
                'and I am a ' + this.occupation + '.');
        }
        else {
            console.log('Hi! I\'m ' + this.name + ' the ' + this.occupation + 
                '. Have a nice ' + timeOfDay);
        }
    }
}

var emily = {
    name: 'Emily',
    age: 19,
    occupation: 'designer'
}

/*
Now let's assume we want Emily to greet us and introduce herself just like John.
We could invoke inheritence and make them of the same object type just like we
did in the last lecture, but what if we wanted to do it only just this once?
Thankfully, js allows us to 'borrow' a method from John in something that's
called - you guessed it - "method borrowing". 
*/

john.greeting.call(emily, 'formal', 'evening');

/*
I'd like to remind you that every function in js is an object. And, as such they
also have their own special functions. call(), for example, allowes us to call
the function with a customly specified value of 'this'. Normally, if we were to
call john.greeting() 'this' would have been equal to 'john'. But call() makes
'this' equal to the first argument. In this case - 'emily'. So 'this.name' in
the funciton definition transforms to 'emily.name'. The rest of the arguments
are passed to the functions as arguments that it needs to run successfully.

But wait. There's more! Not only can you call a function with an arbitry value
of 'this', you can also generate custom versions of your functions using
something that is called currying. Currying is enabled by the 'bind' method.
*/

var johnFrendly = john.greeting.bind(john, 'friendly');
var emilyFriendlyAfternoon = john.greeting.bind(emily, 'friendly', 'afternoon');


johnFrendly('morning');
emilyFriendlyAfternoon();

/*
How cool is that! Here we have generated custom versions of the 'greeting'
function with a preset value of 'this' and preset values of its agruments. We
could generate all sorts of functions this way and use them instead of specifying
arguments to 'greeting' every time.

Another interesting use for currying would be this:
*/

var array = [10, 20, 30];

function arrayCalc(arr, fn) {
    var resultArray = [];
    for (var i = 0; i < arr.length; i++) {
        resultArray.push(fn(arr[i]));
    return resultArray;
}

function add(toAdd, element) {
    return element + toAdd;
}

/*
Imagine we would want to add 3 to every element of the array. We can't exactly
pass 'add' to arrayCalc like "arrayCalc(array, add)" because it will just return
an array of NaN-s. But we can do this:
*/

var addThree = add.bind(this, 3);
arrayCalc(array, addThree);

/*
Here we don't really care what 'this' is so we just make it equal to the global
'this', which is the global object.

And now I feel you've seen 'this' to become enough different things to understand
the joke: "Sometimes when I'm writing Javascript I want to throw up my hands and
say "this is bullshit!" but I can never remember what "this" refers to"
*/

/*
HOMETASK
--------

Build a quiz game in the console
1. Build a function constructor called Question to describe a question. A
question should include:
a) question itself
b) the answers from which the player can choose the correct one
c) correct answer (I would use a number for this)
2. Create a couple of questions using the constructor
3. Store them all inside an array
4. Select one random question and log it on the console, together with the
possible answers (Hint: write a method for the Question objects for this task).
5. Use the 'prompt' function to ask the user for the correct answer. The user
should input the number of the correct answer such as you displayed it on Task 4.
6. Check if the answer is correct and print to the console whether the answer is
correct ot not.
7. Suppose this code would be a plugin for other programmers to use in their
code. So make sure that all your code is private and doesn't interfere with the
other programmers code.

EXPERT LEVEL
------------
8. After you display the result, display the next random question, so that the
game never ends
9. Be careful: after Task 8, the game literally never ends. So include the
option to quit the game if the user writes 'exit' instead of the answer.
10. Track the user's score. So each time an answer is correct, add 1 point to
the score.
11. Display the score in the console.
*/