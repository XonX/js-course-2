/*
NOTE: if this lesson's layout feels a little different from the previous one
that is because I forgot to send the code to myself after the lesson so I pretty
much had to retype the whole thing. This should account for any inconsistencies
you might encounter.

In this lesson we will do 2 things. First we will display our income and expense
entites onscreen. Then we will do a little QoL (quality of life) improvement by
making it so that input fields get cleared after adding an element as well as
making description input field focused so that the page would be ready to accept
the next budget element.

We do this in functions "addListItem" and "clearFields" respectfuly. As these
are both UI-related actions, both these functions are in UIController. Scroll
there now to explore them.
*/

var budgetController = (function () {
    var Expense = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        }
    };

    return {
        addItem: function (type, des, val) {
            var newItem, ID;

            if (data.allItems[type].length > 0) {
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            } else {
                ID = 0;
            }

            if (type === 'exp') {
                newItem = new Expense(ID, des, val);
            } else if (type === 'inc') {
                newItem = new Income(ID, des, val);
            }

            data.allItems[type].push(newItem);

            return newItem;
        },

        testing: function () {
            console.log(data);
        }
    }
})();

var UIController = (function () {
    var DOMstrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: '.income__list',
        expenseContainer: '.expenses__list'
    };

    return {
        getInput: function () {
            return {
                type: document.querySelector(DOMstrings.inputType).value,
                description: document.querySelector(DOMstrings.inputDescription).value,
                value: document.querySelector(DOMstrings.inputValue).value
            }
        },

        getDOMstrings: function () {
            return DOMstrings;
        },

        addListItem: function (obj, type) {
            var html, newHtml, element;
/*
This function accepts an object that is expected to be either an Expense or
Income from the BudgetController and a string indicating what exactly we're
dealing with.
*/
            //1) create HTML string with placeholder text
            if (type === 'inc') {
/*
The HTML here and in the 'exp' case is copied from the html page. It has been
pre-made for us, so we'll just copy it over. If you want to see where the code
came from, search the index.html file for commented out divs with id="income-0"
and "expense-0". You can also try uncommenting them to see them as static
elements on the page.

Notice that we have modified the markup somewhat. There's "income-%id%" instead
of "income-0" and you can also see stuff like "%description%" and "%value%".
These are placeholders that we are going to replace with actual input values.
It's not a strictly defined syntax. We could have done something like "#id#" or
"{id}" just as well, but the one with the '%' signs is one of more widely
accepted ones, so that is what we'll be using.
*/
                html = '<div class="item clearfix" id="income-%id%">\n' +
                    '  <div class="item__description">%description%</div>\n' +
                    '    <div class="right clearfix">\n' +
                    '      <div class="item__value">+ %value%</div>\n' +
                    '      <div class="item__delete">\n' +
                    '        <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\n' +
                    '      </div>\n' +
                    '    </div>\n' +
                    '  </div>';
/*
Notice that we've added strings for incomeContainer and expenseContainer to
DOMstrings since last time. These will be used to select elements into which we
will insert our html.
*/
                element = DOMstrings.incomeContainer
            } else if (type === 'exp') {
                html = '<div class="item clearfix" id="expense-%id%">\n' +
                    '    <div class="item__description">%description%</div>\n' +
                    '    <div class="right clearfix">\n' +
                    '        <div class="item__value">- %value%</div>\n' +
                    '        <div class="item__percentage">21%</div>\n' +
                    '        <div class="item__delete">\n' +
                    '            <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\n' +
                    '        </div>\n' +
                    '    </div>\n' +
                    '</div>';
                element = DOMstrings.expenseContainer;
            }
/*
The replace() function returns the string it has been called on with character
sequence provided as first argument replaced by second argument. This is how we
get our string to contain our actual input data instead of placeholders.
*/
            //2) replace the placeholder with actual data
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', obj.value);
            //3) insert resulting HTML into DOM
/*
InsertAdjacentHTML() is what we use to add html to our webpage. I suggest
checking out the reference page for this function:
https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentHTML
Make sure you understand different options for the first argument. For example,
how would our app's behaivour differ if we used 'afterbegin' instead of 'beforeend'?
*/
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },

        clearFields: function () {
            var fields, fieldsArr;
/*
Here we use querySelectorAll to get both input fields as one object.
*/
            fields = document.querySelectorAll(DOMstrings.inputDescription + ', ' + DOMstrings.inputValue);
/*
We would love for "fields" to be an array, but unfortunately what we get is a
NodeList type object. So we need to do a little trick to convert one to the other.
First, we need to get access to slice() function. This is a function that all
arrays have which, when called without arguments, returns the array itself.
So we would have
arrayVar.slice() => arrayVar
NodeList doesn't have this function, so we have to go to Array.prototype to get
it: Array.prototype.slice.
All that's left is to get this method to somehow execute for fields variable.
In one of previous lessons we have learned about method borrowing, which
enables us to do just that. So let's burrow a method.
*/
            fieldsArr = Array.prototype.slice.call(fields);
/*
Now that we have an array, we want to go through it and clear all elements'
values. We could use a for-loop for it, but arrays offer a more convinient way
of doing this via the forEach() function. ForEach() accepts a callback as an
argument and this callback gets executed for every element of the array.
You can pass the array element, index of the element and the array itself to this
callback, just by specifying their names in the correct order. Element comes
first, then index, then array. So if I were to do something like
myArray.forEach(function(el, in, arr)), el would be the element, in would be the
index and arr would be === myArray.

In the example below I don't need the array, so I just ommit the 3d argument.
Strictly speaking, I don't need the index either, so I can delete that also. But
if I wanted to uncomment the console.log I would have to bring index back.
*/
            fieldsArr.forEach(function (c, index) {
                //console.log(index + ': ' + c.value);
                c.value = '';
            });
/*
This line sets the focus to the first element of the array, which we know to be
our description input field.

And that's it. All that's left is to go to controller and check our points 3 and
3.5 to see the methods being called when they should.
*/
            fieldsArr[0].focus();
        }
    }
})();

var controller = (function (budgetCtrl, UICtrl) {
    var setupEventListeners = function () {
        var DOM = UICtrl.getDOMstrings();

        document.querySelector(DOM.inputBtn).addEventListener('click', contrAddItem);
        document.addEventListener('keypress', function (event) {
            if (event.keyCode === 13 || event.which === 13) {
                contrAddItem();
            }
        });
    };


    function contrAddItem() {
        var input, newItem;
        //1) Get input data
        input = UICtrl.getInput();

        //2) add item to budget controller
        newItem = budgetCtrl.addItem(input.type, input.description, input.value);

        //3) add item to the ui
        UICtrl.addListItem(newItem, input.type);

        //3.5) clear input and set focus
        UICtrl.clearFields();

        //4) calculate the budget

        //5) display the budget in the ui
    }

    return {
        init: function () {
            setupEventListeners();
            console.log('Application started');
        }
    }

})(budgetController, UIController);

controller.init();