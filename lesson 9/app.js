/*
In this lesson we will calculate and display percentage values for individual
expense objects. We will heavily use the feature of functions as arguments to
explore how the mechanism of callbacks work.
*/
var budgetController = (function () {
    var Expense = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
/*
We shall store percentages withing expense objects. As with global percentage
value, we will use a special '-1' value for cases when percentage calculation is
impossible.
*/
        this.percentage = -1;

    };

/*
We shall make it so a method in the Expense object handles the percentage
calculation. Method itself mirrors global percentage calculation logic.
*/
   Expense.prototype.calcPercentage = function (totalIncome) {
        if (totalIncome > 0) {
            this.percentage = Math.round((this.value / totalIncome) * 100);
        } else {
            this.percentage = -1;
        }
    };
//we also need a method to get the calculated percentage 
    Expense.prototype.getPercentage = function () {
        return this.percentage;
    };
 
    var Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1
    };
 
    var calculateTotal = function (type) {
        var sum = 0;
        data.allItems[type].forEach(function (cur) {
            sum += cur.value;
        });
        data.totals[type] = sum;
    };
 
    return {
        addItem: function (type, des, val) {
            var newItem, ID;
 
            if (data.allItems[type].length > 0) {
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            } else {
                ID = 0;
            }
 
            if (type === 'exp') {
                newItem = new Expense(ID, des, val);
            } else if (type === 'inc') {
                newItem = new Income(ID, des, val);
            }
 
            data.allItems[type].push(newItem);
 
            return newItem;
        },
 
        testing: function () {
            console.log(data);
        },
 
        calculateBudget: function () {
            //1. calculate total income and expenses
            calculateTotal('exp');
            calculateTotal('inc');
            //2. calculate the budget: income - expenses
            data.budget = data.totals.inc - data.totals.exp;
            //3. calculate the percentage of income we spent
            if (data.totals.inc > 0) {
                data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
            } else {
                data.percentage = -1;
            }
        },
 
        deleteItem: function (type, id) {
            var ids, index;
            ids = data.allItems[type].map(function (current) {
                return current.id;
            });
            index = ids.indexOf(id);
 
            if (index !== -1) {
                data.allItems[type].splice(index, 1);
            }
        },
 /*
This is the function that initiates percentage calculation. What happens here is
that we take the array that contains all of our expense objects and go through
it using the forEach function. And for every element we then invoke the
calcPercentage function
 */
        calculatePercentages: function () {
            data.allItems.exp.forEach(function (cur) {
                cur.calcPercentage(data.totals.inc);
            })
        },

/*
Similarly to previous function, we also go through the array of our Expence
objects. This time however we use the 'map' function, that returns another array
consisting of return values of all the callback invocations
*/
        getPercentages: function () {
            var allPerc = data.allItems.exp.map(function (cur) {
                return cur.getPercentage();
            });
            return allPerc;
        },
 
        getBudget: function () {
            return {
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            }
        }
    }
})();
 
var UIController = (function () {
    var DOMstrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: '.income__list',
        expenseContainer: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        container: '.container',
        expensesPercLabel: '.item__percentage' //new addition for this lesson
    };
 
    return {
        getInput: function () {
            return {
                type: document.querySelector(DOMstrings.inputType).value,
                description: document.querySelector(DOMstrings.inputDescription).value,
                value: parseFloat(document.querySelector(DOMstrings.inputValue).value)
            }
        },
 
        getDOMstrings: function () {
            return DOMstrings;
        },
 
        addListItem: function (obj, type) {
            var html, newHtml, element;
            //1) create HTML string with placeholder text
            if (type === 'inc') {
                html = '<div class="item clearfix" id="inc-%id%">\n' +
                    '  <div class="item__description">%description%</div>\n' +
                    '    <div class="right clearfix">\n' +
                    '      <div class="item__value">+ %value%</div>\n' +
                    '      <div class="item__delete">\n' +
                    '        <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\n' +
                    '      </div>\n' +
                    '    </div>\n' +
                    '  </div>';
                element = DOMstrings.incomeContainer
            } else if (type === 'exp') {
                html = '<div class="item clearfix" id="exp-%id%">\n' +
                    '    <div class="item__description">%description%</div>\n' +
                    '    <div class="right clearfix">\n' +
                    '        <div class="item__value">- %value%</div>\n' +
                    '        <div class="item__percentage">21%</div>\n' +
                    '        <div class="item__delete">\n' +
                    '            <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\n' +
                    '        </div>\n' +
                    '    </div>\n' +
                    '</div>';
                element = DOMstrings.expenseContainer;
            }
            //2) replace the placeholder with actual data
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', obj.value);
            //3) insert resulting HTML into DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },
 
        clearFields: function () {
            var fields, fieldsArr;
            fields = document.querySelectorAll(DOMstrings.inputDescription + ', ' + DOMstrings.inputValue);
            fieldsArr = Array.prototype.slice.call(fields);
            fieldsArr.forEach(function (c, index) {
                c.value = '';
            });

            fieldsArr[0].focus();
        },
 
        deleteListItem: function (selectorId) {
            var el = document.getElementById(selectorId);
            el.parentNode.removeChild(el);
        },
 /*
This is one of the more tricky functions, so make sure you pay very close attention
to what happens. First, we get a NodeList containing elements that should contain the
percentages. We are making this app compatible with ES5 standard and in ES5
NodeList did not yet have a forEach method. We could use 'slice' to convert the
NodeList to an array as we did previously (check out previous lessons if you need
to refresh your memory). That solution, however, was a hack. More proper solution
would be to write our own forEach implementation.
 */
        displayPercentages: function(percentages) {
            var fields = document.querySelectorAll(DOMstrings.expensesPercLabel);
            // console.log(fields);
 
/*
This is the forEach implementation for a NodeList. It expects a list and a callback.
It goes through the list using a for loop and calls the callback providing it the
current element and the index of said element.
We could've just done percentage displaying logic right here, but this generic
implementation has an advantage of being completely reusable. We can use it on
any NodeList and provide any callback in some other place or, indeed, some other
project.
*/
            var nodeListForEach = function (list, callback) {
                for (var i = 0; i < list.length; i++) {
                    callback(list[i], i);
                }
            };
 
/*
Here we see how the nodeListForEach function is supposed to be used. The callback
provided has access to the 'percentages' array which contains correct values in
correct order and assgnes said values to Node elements one by one.
*/
            nodeListForEach(fields, function (current, index) {
                if (percentages[index] > 0) {
                    current.textContent = percentages[index] + '%';
                } else {
                    current.textContent = '---';
                }
            })
/*
Again, this function is quite tricky and it's ok if you don't quite understand
what's going on at first. Don't hesitate to come over and ask some quiestions.
Also try putting a 'breakpoint' keyword at the start of the function and using
browser developer tools to walk through the function execution step by step.
*/
        },
 
        displayBudget: function (obj) {
            document.querySelector(DOMstrings.budgetLabel).textContent = obj.budget;
            document.querySelector(DOMstrings.incomeLabel).textContent = obj.totalInc;
            document.querySelector(DOMstrings.expensesLabel).textContent = obj.totalExp;
            if (obj.percentage > 0) {
                document.querySelector(DOMstrings.percentageLabel).textContent = obj.percentage + '%';
            } else {
                document.querySelector(DOMstrings.percentageLabel).textContent = '---';
            }
        }
    }
})();
 
var controller = (function (budgetCtrl, UICtrl) {
    var setupEventListeners = function () {
        var DOM = UICtrl.getDOMstrings();
 
        document.querySelector(DOM.inputBtn).addEventListener('click', contrAddItem);
        document.addEventListener('keypress', function (event) {
            if (event.keyCode === 13 || event.which === 13) {
                contrAddItem();
            }
        });
        document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem);
    };
 
    var updateBudget = function () {
        var budget;
       //1. calculate the budget
        budgetCtrl.calculateBudget();
       //2. return the budget
        budget = budgetCtrl.getBudget();
        // console.log(budget);
        //3. display the budget in the ui
        UICtrl.displayBudget(budget);
    };
 
    function contrAddItem() {
        var input, newItem;
        //1) Get input data
        input = UICtrl.getInput();
 
        if(input.description !== '' && !isNaN(input.value) && input.value > 0) {
            //2) add item to budget controller
            newItem = budgetCtrl.addItem(input.type, input.description, input.value);
 
            //3) add item to the ui
            UICtrl.addListItem(newItem, input.type);
            // console.log(newItem);
            //3.5) clear input and set focus
            UICtrl.clearFields();
 
            updateBudget();
 
            updatePercentages(); //we want percentages updated when we add an item
        }
    }
 
    function ctrlDeleteItem(event) {
        var itemID, splitID, type, ID;
 
        itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
        if (itemID) {
            splitID = itemID.split('-');
 
            type = splitID[0];
            ID = parseInt(splitID[1]);
 
            //1. delete the item from data structure
            budgetCtrl.deleteItem(type, ID);
            //2. delete the item from the UI
            UICtrl.deleteListItem(itemID);
            //3. update and show the new budget
            updateBudget();
 
            updatePercentages(); //we want percentages updated when we delete an item
        }
    }
//and here's the function that puts it all together
    function updatePercentages() {
        //1) calculate percentages
        budgetCtrl.calculatePercentages();
        //2) read percentages from the budget controller
        var percentages = budgetCtrl.getPercentages();
        // console.log(percentages);
        //3) update the UI
        UICtrl.displayPercentages(percentages);
    }

    return {
        init: function () {
            setupEventListeners();
            UICtrl.displayBudget({budget: 0, totalExp: 0, totalInc: 0, percentage: -1});
            console.log('Application started');
        }
    }
})(budgetController, UIController);


controller.init();