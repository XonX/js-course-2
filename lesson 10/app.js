/*
In the previous lessons we've actually finished all of the planned functionality.
Today we'll do some quality of life improvements to make our app look more
polished.

Incidentally, this will be your homework for this time. Think of a feature or
a QoL improvement and try to implement it. Get creative!
*/
var budgetController = (function () {
    var Expense = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
        this.percentage = -1;
    };

    Expense.prototype.calcPercentage = function (totalIncome) {
        if (totalIncome > 0) {
            this.percentage = Math.round((this.value / totalIncome) * 100);
        } else {
            this.percentage = -1;
        }
    };

    Expense.prototype.getPercentage = function () {
        return this.percentage;
    };

    var Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1
    };

    var calculateTotal = function (type) {
        var sum = 0;
        data.allItems[type].forEach(function (cur) {
            sum += cur.value;
        });
        data.totals[type] = sum;
    };

    return {
        addItem: function (type, des, val) {
            var newItem, ID;

            if (data.allItems[type].length > 0) {
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            } else {
                ID = 0;
            }

            if (type === 'exp') {
                newItem = new Expense(ID, des, val);
            } else if (type === 'inc') {
                newItem = new Income(ID, des, val);
            }

            data.allItems[type].push(newItem);

            return newItem;
        },

        testing: function () {
            console.log(data);
        },

        calculateBudget: function () {
            //1. calculate total income and expenses
            calculateTotal('exp');
            calculateTotal('inc');
            //2. calculate the budget: income - expenses
            data.budget = data.totals.inc - data.totals.exp;
            //3. calculate the percentage of income we spent
            if (data.totals.inc > 0) {
                data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
            } else {
                data.percentage = -1;
            }
        },

        deleteItem: function (type, id) {
            var ids, index;

            ids = data.allItems[type].map(function (current) {
                return current.id;
            });

            index = ids.indexOf(id);

            if (index !== -1) {
                data.allItems[type].splice(index, 1);
            }
        },

        calculatePercentages: function () {
            data.allItems.exp.forEach(function (cur) {
                cur.calcPercentage(data.totals.inc);
            })
        },

        getPercentages: function () {
            var allPerc = data.allItems.exp.map(function (cur) {
                return cur.getPercentage();
            });
            return allPerc;
        },

        getBudget: function () {
            return {
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            }
        }
    }
})();

var UIController = (function () {
    var DOMstrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: '.income__list',
        expenseContainer: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        container: '.container',
        expensesPercLabel: '.item__percentage',
        dateLabel: '.budget__title--date'
    };
/*
First improvement is formatting the numbers so that they look like currency and
have a separating comma between the hundreds and the thousands.
*/
    var formatNumber = function(num, type) {
        var numSplit, int, dec, sign;

        num = Math.abs(num); //first we use abs to make sure that we have a number type variable
        num = num.toFixed(2); //this turns the number to a string that has a fixed amount of digits after the decimal point 

        numSplit = num.split('.');//we separate the integer from the decimal part for ease of manipulation
        int = numSplit[0];

        if(int.length > 3) {
/*
Here we use substr to split the string between the hundreds and the thousands.
substr(startPos, length) works as follows: it returns a substring of the string
it is called on starting with the character at position startPos and continuing
for length characters.
So 'abcdef'.substr(1, 3) would return 'bcd' (because first letter is at position
0).
We want to separate the last three characters of the string, so we use
'int.length - 3' to find the correct position.

The rest of the code should be self-explanatory.
*/
            int = int.substr(0, int.length - 3) + ',' + int.substr(int.length - 3, 3);
        }

        dec = numSplit[1];
        type === 'exp' ? sign = '-' : sign = '+';

        return sign + ' ' + int + '.' + dec;
    };
/*
Remember this method? It used to be inside 'displayPercentages'. Well now we
found a case for it to be reused, so we made it privately available everywhere
in UIController.
*/
    var nodeListForEach = function (list, callback) {
        for (var i = 0; i < list.length; i++) {
            callback(list[i], i);
        }
    };

    return {
        getInput: function () {
            return {
                type: document.querySelector(DOMstrings.inputType).value,
                description: document.querySelector(DOMstrings.inputDescription).value,
                value: parseFloat(document.querySelector(DOMstrings.inputValue).value)
            }
        },

        getDOMstrings: function () {
            return DOMstrings;
        },

        addListItem: function (obj, type) {
            var html, newHtml, element;
            //1) create HTML string with placeholder text
            if (type === 'inc') {
                html = '<div class="item clearfix" id="inc-%id%">\n' +
                    '  <div class="item__description">%description%</div>\n' +
                    '    <div class="right clearfix">\n' +
                    '      <div class="item__value">%value%</div>\n' +
                    '      <div class="item__delete">\n' +
                    '        <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\n' +
                    '      </div>\n' +
                    '    </div>\n' +
                    '  </div>';
                element = DOMstrings.incomeContainer
            } else if (type === 'exp') {
                html = '<div class="item clearfix" id="exp-%id%">\n' +
                    '    <div class="item__description">%description%</div>\n' +
                    '    <div class="right clearfix">\n' +
                    '        <div class="item__value">%value%</div>\n' +
                    '        <div class="item__percentage">21%</div>\n' +
                    '        <div class="item__delete">\n' +
                    '            <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\n' +
                    '        </div>\n' +
                    '    </div>\n' +
                    '</div>';
                element = DOMstrings.expenseContainer;
            }
            //2) replace the placeholder with actual data
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', formatNumber(obj.value, type)); //using our new function to format the number
            //3) insert resulting HTML into DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },

        clearFields: function () {
            var fields, fieldsArr;
            fields = document.querySelectorAll(DOMstrings.inputDescription + ', ' + DOMstrings.inputValue);
            fieldsArr = Array.prototype.slice.call(fields);
            fieldsArr.forEach(function (c) {
                c.value = '';
            });

            fieldsArr[0].focus();
        },

        deleteListItem: function (selectorId) {
            var el = document.getElementById(selectorId);
            el.parentNode.removeChild(el);
        },

        displayPercentages: function(percentages) {
            var fields = document.querySelectorAll(DOMstrings.expensesPercLabel);

            nodeListForEach(fields, function (current, index) {
                if (percentages[index] > 0) {
                    current.textContent = percentages[index] + '%';
                } else {
                    current.textContent = '---';
                }
            })
        },

        displayBudget: function (obj) {
            var type = obj.budget > 0 ? 'inc' : 'exp';
            //Using formattet numbers for budget also. We need type for the correct signs in front of numbers
            document.querySelector(DOMstrings.budgetLabel).textContent = formatNumber(obj.budget, type);
            document.querySelector(DOMstrings.incomeLabel).textContent = formatNumber(obj.totalInc, 'inc');
            document.querySelector(DOMstrings.expensesLabel).textContent = formatNumber(obj.totalExp, 'exp');
            if (obj.percentage > 0) {
                document.querySelector(DOMstrings.percentageLabel).textContent = obj.percentage + '%';
            } else {
                document.querySelector(DOMstrings.percentageLabel).textContent = '---';
            }
        },
/*
Another improvement is displaying current month and year at the top of the page.
*/
        displayDate: function () {
            var now, year, month, monthName;

            monthName = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                'October', 'November', 'December'];
//this is how we can get a representation of the current moment in time
            now = new Date();
/*
It is, of course, also possible to get a representation of an arbitrary moment in
time. For example, if we wanted to get a representation of this year's Estonian
Independance Day (24 February) we would do it like this

var indDay = new Date(2020, 1, 24);

Note that we used '1' for February. That is because in js months, like arrays,
start at zero. Not days though. They still start at 1.
Consistency.
*/
            year = now.getFullYear();
            month = now.getMonth();
//one positive effect that comes from js months starting at zero is that
//we can do something like this with our monthName array
            document.querySelector(DOMstrings.dateLabel).textContent = monthName[month] + ' ' + year;
        },
/*
This function toggles css classes that make input fields and input button turn
red. This is the second place we use nodeListForEach function. We tie this to the
'change' event for the income-expence dropdown down in the setUpEventListeners
function.

Because the dropdown only has two values and we know we start at 'income', we
don't really have to check which value is currently selected and just toggling
classes is sufficient.
*/
        changeType: function () {
            var fields = document.querySelectorAll(
                DOMstrings.inputType + ',' +
                DOMstrings.inputDescription + ',' +
                DOMstrings.inputValue
            );

            nodeListForEach(fields, function (cur) {
                cur.classList.toggle('red-focus');
            });

            document.querySelector(DOMstrings.inputBtn).classList.toggle('red');
        }
    }
})();

var controller = (function (budgetCtrl, UICtrl) {
    var setupEventListeners = function () {
        var DOM = UICtrl.getDOMstrings();

        document.querySelector(DOM.inputBtn).addEventListener('click', contrAddItem);
        document.addEventListener('keypress', function (event) {
            if (event.keyCode === 13 || event.which === 13) {
                contrAddItem();
            }
        });
        document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem);
//tie 'changeType' to 'change' event
        document.querySelector(DOM.inputType).addEventListener('change', UICtrl.changeType);
    };

    var updateBudget = function () {
        var budget;
       //1. calculate the budget
        budgetCtrl.calculateBudget();
       //2. return the budget
        budget = budgetCtrl.getBudget();
        // console.log(budget);
       //3. display the budget in the ui
        UICtrl.displayBudget(budget);
    };


    function contrAddItem() {
        var input, newItem;
        //1) Get input data
        input = UICtrl.getInput();

        if(input.description !== '' && !isNaN(input.value) && input.value > 0) {
            //2) add item to budget controller
            newItem = budgetCtrl.addItem(input.type, input.description, input.value);

            //3) add item to the ui
            UICtrl.addListItem(newItem, input.type);
            // console.log(newItem);
            //3.5) clear input and set focus
            UICtrl.clearFields();

            updateBudget();

            updatePercentages();
        }
    }

    function ctrlDeleteItem(event) {
        var itemID, splitID, type, ID;

        itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
        if (itemID) {
            splitID = itemID.split('-');

            type = splitID[0];
            ID = parseInt(splitID[1]);

            //1. delete the item from data structure
            budgetCtrl.deleteItem(type, ID);
            //2. delete the item from the UI
            UICtrl.deleteListItem(itemID);
            //3. update and show the new budget
            updateBudget();

            updatePercentages();
        }
    }

    function updatePercentages() {
        //1) calculate percentages
        budgetCtrl.calculatePercentages();
        //2) read percentages from the budget controller
        var percentages = budgetCtrl.getPercentages();
        // console.log(percentages);
        //3) update the UI
        UICtrl.displayPercentages(percentages);
    }

    return {
        init: function () {
            setupEventListeners();
            UICtrl.displayBudget({budget: 0, totalExp: 0, totalInc: 0, percentage: -1});
//we want displayDate to be executed when the app is started, so we put it into 'init'
            UICtrl.displayDate();
            console.log('Application started');
        }
    }

})(budgetController, UIController);

controller.init();