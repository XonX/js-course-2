/*
Today we are going to learn about destructuring. Destructuring is a process
opposite to structuring. Which in turn is a process of uniting different values
into neat structures.
So, put simply, destructuring is a process of taking a structure (such as an
array or an object) and storing its' values into separate variables.
*/
//ES5
var john = ['John', 28];
var name5 = john[0];
var age5 = john[1];

/*
While in ES5 we can only access array members one by one, ES6 provides a more
clear and convinient way to assign array members to individual variables.
*/
//ES6
const [name6, age6] = john;
console.log(name6, age6);

const obj = {
    firstName: 'John',
    lastName: 'Smith'
}
/*
We can also do the same with objects. Objects don't have a garanteed order of
fields, so we can only reference them by name. So we either make our individual
variable names the same as field names like shown below, or...
*/
const {firstName, lastName} = obj;
console.log(firstName, lastName);

/*
...we use the following notation to make variables have arbitrary names.
*/
const {firstName : a, lastName : b} = obj;
console.log(a, b);

/*
Destructuring can be quite useful when we have a function that returns a
structure (array or object) and we want to store the return values separately.
See below.
*/
function calcLegalAge(year) {
    const age = new Date().getFullYear() - year;
    return [age, 18 <= age];
}

const [age, isLegal] = calcLegalAge(1990);
console.log(age);
console.log(isLegal);

/*
Let's turn our attention to the boxes in our HTML. Let's say that we want to
paint them blue and then make those that actually changed color (as we already
have one blue box) to indicate it, by changing their text too.

Changing the color is quite straightforward and ES6 turns it itno a neat
one-liner. ES5 option commented out to avoid conflicts.
*/
const boxes = document.querySelectorAll('.box');
//ES5
// var boxesArr5 = Array.prototype.slice.call(boxes);
// boxesArr5.forEach(function (cur) {
//     cur.style.backgroundColor = 'dodgerBlue';
// })

//ES6
boxes.forEach(cur => cur.style.backgroundColor = 'dodgerBlue');

/*
Changing the text is a little trickier, because there is a box we have to skip.
Loop iterations can be skipped using the 'continue' keyword. Unfortunately,
forEach functions both in ES5 and ES6 don't support 'break' or 'continue'. So
we need to write a loop that would iterate over the elements of the collection.
ES5 loops should already be familiar to you.
*/
//ES5
// for(var i = 0; i < boxesArr5.length; i++) {
//     if (boxesArr5[i].className === 'box blue') {
//         continue;
//     }
//     boxesArr5[i].textContent = 'I changed to blue';
// }

/*
As usual, ES6 has a more convinient way of doing things. Behold the for-of
loop. It's similar in function to the foreach loop from Java, if that's
familiar to you. It iterates through the structure provided and executes the
code for every element.
*/
//ES6
for (const cur of boxes) {
    if (cur.className.includes('blue')) {
        continue;
    }
    cur.textContent = 'I changed to blue';
}
/*
One last thing to look at today are a couple of very useful methods ES6 got for
arrays. Let's say that we have an array of ages and we want to know if any of
the ages are above the legal age, where in the array the element is (its index)
and the element's value. To do that in ES5 we first need to map the array to
an array of booleans and then use .indexOf(true) to find out the index of the
target element.
*/
var ages = [12, 17, 21, 2, 10];

//ES5
var legal = ages.map(function (cur) {
    return cur >= 18;
})
console.log(legal);
console.log(ages[legal.indexOf(true)])

/*
ES6 brings us 'findIndex' and 'find' methods. Using those we can get rid of the
intermediate array and search the array itself for the element we want.
*/
//ES6
console.log(ages.findIndex(cur => cur >= 18));
console.log(ages.find(cur => cur >= 18));