/*
In this lesson we will finish the initial to-do list we have set for ourselves
within the contrAddItem function. Comments will highlight differences between
this and the previous versions of code.
*/
var budgetController = (function () {
    var Expense = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
/*
New additions to data are budget and percentage variables. Budget is
self-explanatory as the amonut of money left after all the expenses. Percentage
will show us how many procents of the total income we have spent. You may notice
that unlike all the other variables, percentage is initialized to -1. -1 is a
special value that indicates that a percentage has not yet been calculated so we
won't show it.
*/
        budget: 0,
        percentage: -1
    };

/*
This function calculates the total budget. It should be quite straightforward for
you if you have been following the previous lessons. We go through an array
representing a specific type, add up all the values in the 'sum' variable and
save it to the corresponding 'totals' variable
*/
    var calculateTotal = function (type) {
        var sum = 0;
        data.allItems[type].forEach(function (cur) {
            sum += cur.value;
        });
        data.totals[type] = sum;
    };

    return {
        addItem: function (type, des, val) {
            var newItem, ID;

            if (data.allItems[type].length > 0) {
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            } else {
                ID = 0;
            }

            if (type === 'exp') {
                newItem = new Expense(ID, des, val);
            } else if (type === 'inc') {
                newItem = new Income(ID, des, val);
            }

            data.allItems[type].push(newItem);

            return newItem;
        },

        testing: function () {
            console.log(data);
        },

/*
This function does the budget calculation. It first calls calculateTotal that
you have already seen above for both expenses and incomes. It then uses the
calculated totals to calculate the budget and percentage. Budget is straight
enough, but percentages are trickier.

First, we want to make sure we don't divide by zero, so we should check that
income is not zero. Otherwize, if we just do the calculation, we might get
percentage === Infinity. Comment out the if statement and the else block,
uncomment the console.log statement and try entering some expenses without any
incomes to see this case in action.

If income is zero, we set percentage to '-1' as this is, as mentioned above, a
special value indicating that a percentage has not been calculated.
*/
        calculateBudget: function () {
            //1. calculate total income and expenses
            calculateTotal('exp');
            calculateTotal('inc');
            //2. calculate the budget: income - expenses
            data.budget = data.totals.inc - data.totals.exp;
            //3. calculate the percentage of income we spent
            if (data.totals.inc > 0) {
                data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
            } else {
                data.percentage = -1;
            }

            // console.log(data);
        },
/*
A simple function that returns an object containing all of the budget data that
we need outside the budgetController so far.
*/
        getBudget: function () {
            return {
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            }
        }
    }
})();

var UIController = (function () {
    var DOMstrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: '.income__list',
        expenseContainer: '.expenses__list',
/*
We added some more strings to reference parts of html that we are going to modify
*/
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage'
    };

    return {
        getInput: function () {
            return {
                type: document.querySelector(DOMstrings.inputType).value,
                description: document.querySelector(DOMstrings.inputDescription).value,
/*
Here we wrapped the value in parseFloat function. We need to do this because
otherwise 'value' will be of type String and that may cause type issues when we
perform arithmetic operations with the values.
*/
                value: parseFloat(document.querySelector(DOMstrings.inputValue).value)
            }
        },

        getDOMstrings: function () {
            return DOMstrings;
        },

        addListItem: function (obj, type) {
            var html, newHtml, element;
            //1) create HTML string with placeholder text
            if (type === 'inc') {
                html = '<div class="item clearfix" id="income-%id%">\n' +
                    '  <div class="item__description">%description%</div>\n' +
                    '    <div class="right clearfix">\n' +
                    '      <div class="item__value">+ %value%</div>\n' +
                    '      <div class="item__delete">\n' +
                    '        <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\n' +
                    '      </div>\n' +
                    '    </div>\n' +
                    '  </div>';
                element = DOMstrings.incomeContainer
            } else if (type === 'exp') {
                html = '<div class="item clearfix" id="expense-%id%">\n' +
                    '    <div class="item__description">%description%</div>\n' +
                    '    <div class="right clearfix">\n' +
                    '        <div class="item__value">- %value%</div>\n' +
                    '        <div class="item__percentage">21%</div>\n' +
                    '        <div class="item__delete">\n' +
                    '            <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\n' +
                    '        </div>\n' +
                    '    </div>\n' +
                    '</div>';
                element = DOMstrings.expenseContainer;
            }
            //2) replace the placeholder with actual data
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', obj.value);
            //3) insert resulting HTML into DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },

        clearFields: function () {
            var fields, fieldsArr;
            fields = document.querySelectorAll(DOMstrings.inputDescription + ', ' + DOMstrings.inputValue);
            fieldsArr = Array.prototype.slice.call(fields);
            fieldsArr.forEach(function (c, index) {
                c.value = '';
            });

            fieldsArr[0].focus();
        },

/*
This is the function we use to update the UI of our app with the new budget
values. We don't expect to have any HTML in our values, so we use the textContent
property.

Again, percentages are a little special. For one, we want to add the percent
sign to them, hence the "+ '%". And also, we don't always have a calculated
percentage. Remember the special cases we marked as '-1'? In those cases we want
to indicate that percentage is not available, so we put '---' as a value.
*/
        displayBudget: function (obj) {
            document.querySelector(DOMstrings.budgetLabel).textContent = obj.budget;
            document.querySelector(DOMstrings.incomeLabel).textContent = obj.totalInc;
            document.querySelector(DOMstrings.expensesLabel).textContent = obj.totalExp;
            if (obj.percentage > 0) {
                document.querySelector(DOMstrings.percentageLabel).textContent = obj.percentage + '%';
            } else {
                document.querySelector(DOMstrings.percentageLabel).textContent = '---';
            }
        }
    }
})();

var controller = (function (budgetCtrl, UICtrl) {
    var setupEventListeners = function () {
        var DOM = UICtrl.getDOMstrings();

        document.querySelector(DOM.inputBtn).addEventListener('click', contrAddItem);
        document.addEventListener('keypress', function (event) {
            if (event.keyCode === 13 || event.which === 13) {
                contrAddItem();
            }
        });
    };

/*
This function implements what used to be points 4 and 5 from the todo list in
contrAddItem. The reason we put these points into a separate function is because
in the future, when we implement item deletion, we would want to redo just those
steps to recalculate the budget. So we setting our code up to be reused.

You have already seen implementations of all the functions used here
*/
    var updateBudget = function () {
        var budget;
       //1. calculate the budget
        budgetCtrl.calculateBudget();
       //2. return the budget
        budget = budgetCtrl.getBudget();
        // console.log(budget);
       //3. display the budget in the ui
        UICtrl.displayBudget(budget);
    };


    function contrAddItem() {
        var input, newItem;
        //1) Get input data
        input = UICtrl.getInput();

/*
Notice that everything beyond 1. is now wrapped in an if statement. This is done
for input validation purposes. Without it, we could have just press the button
with empty input fields and get garbage budget items with no values. This
statement checks that we have a description, that a value is entered (isNaN
checks if a value is - you guessed it - NaN. Which is a special value in js that
stands for Not a Number. Try executing 'typeof NaN' for something amusing), and
that the value entered is a positive number. We can't exactly spend -30 euros or
earn -50.
*/
        if(input.description !== '' && !isNaN(input.value) && input.value > 0) {
            //2) add item to budget controller
            newItem = budgetCtrl.addItem(input.type, input.description, input.value);

            //3) add item to the ui
            UICtrl.addListItem(newItem, input.type);

            //3.5) clear input and set focus
            UICtrl.clearFields();

            //4) calculate the budget
            //5) display the budget in the ui
            updateBudget();
        }
    }

    return {
        init: function () {
            setupEventListeners();
/*
One last thing to do is to reset values put as placeholders in our HTML. To do
that, we call displayBudget function with initial values in the init function.
*/
            UICtrl.displayBudget({budget: 0, totalExp: 0, totalInc: 0, percentage: -1});
            console.log('Application started');
        }
    }

})(budgetController, UIController);

controller.init();

/*
HOMETASK

Let's make our input fields a little fancier. Make it impossible to have a
negative value in the value field. For example, if I try to enter '-13' the field
should reset to zero. Also if I try to use arrows (should be present for value
field in most browsers) to go below zero, it souldn't let me.

Hint: you will most probably have to listen for some event and set the value to
zero for some condition
*/