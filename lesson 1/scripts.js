//The topic of this lesson is object-oriented programming

// primitives: numbers, strings, booleans, undefined, null
// objects: everything else

var CURRENT_YEAR = 2019;

//this is the easiest and most popular way to create objects.

//first, we create a function constructor:
var Person = function(name, yearOfBirth) {
       this.name = name;
       this.yearOfBirth = yearOfBirth;
       //below is one of the ways we can add functions to our objects
       //while completely valid, this is not very memory efficient and should
       //be avoided unless completely necessary
       //this.calculateAge = function() {
              //return CURRENT_YEAR - this.yearOfBirth;
       //}
}

//and then we use it to create objects of type "Person"
//var john = new Person('John', 1989);
//var mary = new Person('Mary', 1995);


//this is a much better way to add a function to an object. This way, the function
//will be saved in the shared __proto__ property of every Person type object we create.
//Since it is shared, the function won't be duplicated in memory
Person.prototype.calculateAge = function() {
       return CURRENT_YEAR - this.yearOfBirth;
}

var john = new Person('John', 1989);
var mary = new Person('Mary', 1995);
//at this point I highly recommend to pull up a js console in the browser and play around
//with objects and take a look at their __proto__ property
 

//new keyword: "new" :)
//1) creates an empty object (var obj = {})
//2) the constructor function is called with this = obj.
//if you remember from previous lessons, if a function is called without an associated object
//this === global context. Using "new" you can override this behaivour

//functions aren't the only thing that can be stored in a prototype. Variables work too
Person.prototype.lastName = 'Smith';

//hasOwnProperty is a way to check if a specific property is really a part of the object itself
//or is inhereted through the prototype chain
console.log(john.hasOwnProperty('name')); //returns true
console.log(john.hasOwnProperty('lastName')); //returns false, since the property is actually in __proto__

console.log(john instanceof Person); //a way to check if an object is of a specific type
console.log(john instanceof Object); //all objects in js inherit from the Object

console.log([9, 8, 7]);
console.info([9, 8, 7]); //in some browser console this provides a more roubust output that allows to explore an array as an object
//Since Array is not a primitive, it is also an object. If you explore its __proto__ property, you will find all the familiar
//array functions that you've previously used in our lessons, such as "indexOf", "push", "pop" etc

//Now you should know how this works :)
var arr = new Array(4, 3, 2);

 
//here is an alternative way to create an object from a prototype. It's much more bulky, so it's not used as often
//it does, however, allow to create complex inheritense trees so you might still encounter it
var humanProto = {
       calculateAge: function() {
              return CURRENT_YEAR - this.yearOfBirth;
       }
}

var tim = Object.create(humanProto);
tim.name = 'Tim';
tim.yearOfBirth = 1990;

var carl = Object.create(humanProto, {
              name: { value: 'Carl'}, //note that you can not just type "name: 'Carl'". It won't work. Hence, "bulky"
              yearOfBirth: {value: 1993}
});

//this next segment demonstrates the difference between how js treats primitive variable types and objects
//primitive point variable references its value directly, but for object, variable actually contains a pointer
//to a spot in memory, where the object is stored
var num = 3;
num++;

function increment(numberVar) {
       numberVar++;
       return numberVar;
}
//because num is primitive, if we call increment(num), the value of num itself will be unchanged afterwards,
//since a copy of the value of num will be passed directly

var nobj = {val: 3};
function incrementObj(numberObj) {
       numberObj.val++;
       return numberObj;
}
//in this case, however, incrementObj(nobj) will alter the contents of nobj, because a pointer is passed instead of
//a copy of an object


/*
--------
Hometask
--------

Imagine that you are creating an army of robots to take over the world.
Create some robots using a function constructor pattern and use the prepareForBattle function provided to test them
*/

function prepareForBattle(robot) {
       robot.report(); //should print "Robot %robotname% ready for battle!"
}

//Bonus task: the russians have sent a spy disguised as a robot to infiltrate your army and ruin your plans:

var boris = {
       report: function() {
              console.log("Robot Boris ready for battle!");
       }
}

//alter the prepareForBattle function so that it would check if provided robot is really one of your own and catch Boris