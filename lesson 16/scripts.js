/*
In this lesson we shall learn about asynchronous programming. Everything that we
did up to this point was synchronous. Meaning, our programms executed line by
line one after the other and no line got executed before the previous one
finished executing. It's absolutely fine for most cases, but imagine that we
would like to ask a backend server to do some long and computation-heavy process.

If we were to use synchronous programming for that, it would mean that our
webpage would just freeze up, until server responce would be recieved and
processed. Such behaivour for modern web-apps is unacceptable. So we will need to
take a plunge into asynchronous world.
*/
const second = () => {
/*
This is the easiest way to see async programming in action. setTimeout is a way
to tell the application 'execute this code after x milliseconds' where x is the
second argument to the setTimeout function. If you call 'first()' you will see
that 'second' appears in the console after 'The end', which means that calling
setTimeout() does not pause the execution for 2 seconds and the console.log() in
second() gets executed after first() has finished executing.
*/
    setTimeout(() => {
            console.log('second');
        }, 2000);
};

const first = () => {
    console.log('Hello');
    second();
    console.log('The end');
};

first();

/*
It is important to note that setTimeout() is not a part of javascript itself.
It is a part of browser's WEB API and so calls to it are handeled by the browser
itself, outside of the JS engine.
When setTimeout is called, here's what happens:
1) Callback is passed to the API and is saved within it.
3) Timer runs within WEB API for the duration specified
4) When the timer is over, callback gets passed to message queue. This is a
special storage, that JS engine constantly checks in something called 'event
loop'.
5) Event loop checks message queue. When it encounters our callback, it is then
passed to JS engine to be executed
*/

/*
We can nest asynchronous function calls inside eachother. Consider the example
below, where we call setTimeout from within another setTimeout callback.

This, however, is a potentially dangerous way to do things, as nested callbacks
are usually hard to debug, read and understand. They lead to what is known as
'callback hell'.
*/
function getRecipe() {
    setTimeout(() => {
        const recipeIDs = [876, 395, 558, 787];
        console.log(recipeIDs);

        setTimeout(id => {
            const recipe = {
                title: 'Dumplings',
                publisher: 'Juri'
            };
            console.log(`${id}: ${recipe.title}`);

            setTimeout(publisher => {
                const recipe = {
                    title: 'Pizza',
                    publisher: 'Juri'
                };
                console.log(`Another recipe published by ${publisher}: ${recipe.title}`);

            }, 1500, recipe.publisher); //we can pass arguments to callback functions
        }, 1000, recipeIDs[1]);         //by passing them to setTimeout after the timeout limit
    }, 1500)
}

getRecipe();

/*
ES6, thankfully, offers us a way to avoid callback hell, by using something
called a 'Promise'.

Promise:
1. Object that keeps track about whether a certain event has happened or not
2. Determines what happens after the event has happened
3. Implements the concept of a future value that we're expecting

Look below for the simplest definition of a Promise. When defining a Promise, we
need to define the usage of 2 functions within it - 'resolve' and 'reject'. Note
that you don't strictly have to use these specific names, but it's a good idea
to do so.

First function (resolve) is a function that should be executed if code in the
promice executed successfully. The second one (reject) is a sign that something
went wrong. For example, a Promise, containing a call to remote server might
contain something like

const responce = callServer();
if(containsErrors(response)) {
    reject(response)
} else {
    resolve(response)
}

In short, you, as a programmer decide, what constitutes a 'successfull' Promise
by specifying when to call which function.
*/
const getIDs = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve([876, 395, 558, 787]);
    }, 1500);
});

/*
Here's how we define a Promise, if we want to pass a parameter to it. We need to
define it inside a function and then we can pass the paremeter to the function
to be used in the Promise definition.
*/
const getRecipe = recipeID => {
    return new Promise(((resolve, reject) => {
        setTimeout(ID => {
            const recipe = {
                title: 'Dumplings',
                publisher: 'Juri'
            };
            resolve(recipe);
        }, 1500, recipeID)
    }))
};

const getRelated = publisher => {
    return new Promise(((resolve, reject) => {
        setTimeout(pub => {
            const recipe = {
                    title: 'Pizza',
                publisher: 'Juri'
            };
            resolve(`Another recipe published by ${publisher}: ${recipe.title}`);
        }, 1500, publisher);
    }))
};

/*
You resolve the Promise by calling its 'then()' method. Note that we can only
provide one callback function as an argument to it. This is what is going to be
used as a 'resolve' function in the Promice code. If we want to provide a
'reject' function, we will need to call the 'catch()' function.
These function calls can be chained, so that a call to resolve a Promise with
error checking can look someting like

promise.then(resolveFunctionImpl).catch(rejectFunctionImpl)

This line would execute the code inside the Promise 'promise' and if it
encounters a statement to call 'resolve()', it will call 'resolveFunctionImpl()',
and if it encounters a statement to call 'reject()', 'rejectFunctionImpl()' will
be executed.

But how would Promises help us with callback hell? Well, if we want to call
async code within async code, we don't actually need to define a Promise within
a Promise, even if we would need the result of the first Promise to resolve the
second one. This is because the result can be passed to the 'resolve' function
and within our definition of 'resolve' we can then call upon another predifined
promise.
This is what happens in the lines

return getRecipe(IDs[1]);

and

return getRelated(recipe.publisher);

below.

This also means that the call to 'then()' function on the first Promise will
return a second Promise, so we can chain the 'then()' calls too.

It is probably a little hard to wrap your head around, so make sure to play
around with Promises in your spare time and if you're still confused, my skype,
fb, e-mail and telegram are always open to your questions.
*/

getIDs.then(IDs => {
    console.log(IDs);
    return getRecipe(IDs[1]);
}).then(recipe => {
    console.log(`${recipe.title}`);
    return getRelated(recipe.publisher);
}).then(anotherRecipe => {
    console.log(anotherRecipe);
}).catch(error => console.log(error));