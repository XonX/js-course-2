QUnit.module('Controller', (hooks) => {
    let controller, mockGame;
    mockGame = {
        rollCalled: false,
        roll: function () {
            this.rollCalled = true;
        }
        };

    hooks.beforeEach(() => {
        mockGame.rollCalled = false;
        controller = new Controller(mockGame);
    });

    QUnit.test('score cleared when "Submit" pressed', (assert) => {
        document.getElementById('qunit-fixture').innerHTML = '<input type="number" id="roll-score"><input type="button" id="submit-score" value="Submit">';
        document.getElementById('roll-score').value = 3;

        controller.init();
        clickButtonById('submit-score');
        assert.equal(document.getElementById('roll-score').value, '');
    });

    QUnit.test('roll called when "Submit" pressed', (assert) => {
        debugger;
        document.getElementById('qunit-fixture').innerHTML = '<input type="number" id="roll-score"><input type="button" id="submit-score" value="Submit">';
        controller.init();
        clickButtonById('roll-score');

        assert.ok(mockGame.rollCalled);
    });

    function clickButtonById(id) {
        const click = new Event('click');
        document.getElementById(id).dispatchEvent(click);
    }
});

QUnit.module('game', (hooks) => {
    let game;

    hooks.beforeEach(() => {
        game = new Game();
    });

    QUnit.test('game can roll', (assert) => {
        assert.expect(0);
        game.roll();
    });
});