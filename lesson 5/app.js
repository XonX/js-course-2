/*
Now it's time to write some actual code for our app! We shall follow the to-do
list from contrAddItem. This will require jumping around the code a bit, as
different actions require mdifying different parts of the code. So prepare to
scroll a lot.

First item on the agenda is getting the input data from UI. Which should suggest
us that we need to do some work in the UIController. Scroll there now.
*/
var budgetController = (function () {
/*
🚀 The eagle has landed!
Before we'll set out to write the addItem function, we need to do some
preparations first. We will need different kinds of objects for incomes and
expenses. Let's use the function constructor pattern to make them
*/
    var Expense = function(id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var Income = function(id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };
/*
Descriptions and values are self-explanotary, but what is this id? It is a
unique identifier that we are going to use to reference the objects in the
future.

We will also need a way to store all our expenses, incomes, totals etc in the
budgetController. We could, of course, just make separate variables for them like
so:
var allExpenses = [];
var allIncomes = [];
var totalIncome = 0;
etc etc
but it is structurally better and, as we will soon see, more convinient to create
a single structured data structure to store all the data in it
*/
    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        }
    };
/*
Now that the internal workings are in place, we can move on to create addItem.
It, of course, needs to be public
*/
    return {
        addItem: function(type, des, val) {
            var newItem, ID;
/*
We first need to create a way to generate IDs. We generally want IDs to follow
the formula ID = previousID + 1. Naive approach would be to just go
ID = lengthOfCorrespondingArray, but because in the future we will be able to
delete budget entries, this might lead to problems. Imagine that we had an array
with IDs [0 1 2 3 4], and we delete item number 2, so the array becomes
[0 1 3 4]. Now the length of the array is 4, so using that as an ID will lead to
duplication. We'll need something more sophisticated, like the construction below.
Let's pick it apart.

At first, the if condition. It demonstrates why using a data structure is much
more convinient then just separate variables. Because the type can be 'exp' or
'inc' and that is exactly how we marked expenses and incomes in our data structure
we can use data.allItems[type] to get the right array. allItems[type] will
translate to either allItems['inc'] or allItems['exp'], which is one of the ways
to reference object properties in js.

Next line is a little tricky. First, we get the right array using
dataAllitems[type]. Then we find the last element of the array using
data.allItems[type].length - 1, and then we get the ID of said element and add 1
to it, thus increasing ID by one.
*/
            if (data.allItems[type].length > 0) {
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            } else {
/*
If the selected array is empty, the previous code will produce an error. So if
the array is empty, we just set the ID to zero.
*/
                ID = 0;
            }

/*
The rest of the function should be self-explanotary by now. Look through it and
try to understand what's going on yourself
*/
            
            // Create new item based on 'inc' or 'exp' type
            if (type === 'exp') {
                newItem = new Expense(ID, des, val);
            } else if (type === 'inc') {
                newItem = new Income(ID, des, val);
            }
            
            // Push it into our data structure
            data.allItems[type].push(newItem);
            
            // Return the new element
            return newItem;
        }
    }
/*
One last thing that we did in the lesson is create an init function. Init
function is something we have already seen in the previous project, so it's old
knowledge for us. About as old as this statue: 🗿
*/
})();

var UIController = (function () {
/*
The input elements that interest us are <select> with class 'add__type' (which
can be 'exp' for expense or 'inc' for income), and inputs with classes
'add__description' and 'add__value'. Now we could just provide them to
querySelector function as strings, but this is not a good solution. There's no
autocomplere, IDE won't check for typos and if we would, for some reason, want
to change a class name, we would have to go through all of the code and change
every instance by hand. Yes, there's search-and-replace, but it can produce
false positives. Good luck debugging that!

It would be much better to save all of the magic strings into an object, which
we could then reference, whenever we need to. Let's do just that. 
*/
    var DOMstrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
/*
Great. But these are not the first dom strings we've referenced. Remember our
event listeners and '.add__btn'? Let's add that too.
*/
        inputBtn: '.add__btn'
    }
/*
But wait. DOMstrings is a variable private to UIController and thus invisible
outside it. While we need the inputBtn in controller. So we need to expose
DOMstrings to the ouside world
*/
    return {
        getDOMstrings: function() {
            return DOMstrings;
        },
/*
And now, we can get DOMstrings in controller and use them. Scroll until you
encounter a tree like this one: 🌲
*/
/*
🚗 Now that we have saved the DOMStrings, we can use them to get input data from
the UI. The function for that should also be public as we need that data in the
controller. We shall get values from dom elements using querySelector and value
attribute and then return an object that will contain type, description and value
attributes.
*/
        getInput: function() {
            return {
                type: document.querySelector(DOMstrings.inputType).value, // Will be either inc or exp
                description: document.querySelector(DOMstrings.inputDescription).value,
                value: document.querySelector(DOMstrings.inputValue).value
            };
        }
/*
Now all that's left is to call this function in contrAddItem. Follow the white
rabbit to it: 🐇
*/
    };
})();

var controller = (function (budgetCtrl, UICtrl) {
/*
🗿 init function is a function that sets up the whole application and initializes
all of the important stuff needed for it to work. Event listeners are a perfect
example of that, so lets get them all into a function for the init function to
call. This gives us an added bonus of having a specific place to put all future
event listener initializations. So whenever we want to modify an event listener
in the future, we will know exactly where to look.
After you're done inspecting the setupEventListeners function, it's time to go to
the init function itself. As it is the first function to be called to get things
running, it should be exposed to the whole world to see: 🌍
*/
    var setupEventListeners = function() {
/*
🌲 You might notice that event listener setup has moved to a function. Don't
worry about it right now, we'll get to that. For now, just observe how we get
DOMstrings from UICtrl object and use it to set the event listener
*/
        var DOM = UICtrl.getDOMstrings();
    
        document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem);
/*
Isn't it great? Now we're done here for now. Hop in the car to get back to
UIController: 🚗
*/

        document.addEventListener('keypress', function(event) {
            if (event.keyCode === 13 || event.which === 13) {
                ctrlAddItem();
            }
        });
    };

    function contrAddItem() {
        var input;
        
        // 1. Get the field input data
        input = UICtrl.getInput();
/*
🐇 Now that we have the ability to get some data from UI into the controller
via the input object, it is time to use that input data to add an item to the
budget controller. We shall call it here right away as if we have already wrote
it, to minimise the number of hops
*/ 
        //2) add item to budget controller
        budgetCtrl.addItem(input.type, input.description, input.value);
/*
And now get in the rocket, because we need to fly aaaaall the way up to
budgetController: 🚀
*/

        //3) add item to the ui

        //4) calculate the budget

        //5) display the budget in the ui
        console.log('Function called');
    };
/*
🌍 And here we are. It's a simple function, but a very important one. It will
do more things in the future, but even know it is crucial to our app startup.
All that is left is to call it. This will be done outside all controllers in the
global scope.
*/
    return {
        init: function() {
            setupEventListeners();
            console.log('Application has started.');
        }
    }

})(budgetController, UIController);

controller.init()

/*
Since we are VERY in the middle of things, there's nothing to show onscreen yet.
If you really want to see the code in action, I suggest adding a public "testing"
function to budgetController that would do console.log(data) and make sure that
Expense and Income objects do indeed get created and saved in the correct
arrays when the button is pressed.
*/