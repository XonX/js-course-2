/*
This lesson will show a possible solution to the hometask from lesson 16.
As such, there's almost no new material here and comments will only cover that.
If something is still unclear, don't hesitate to contact me.

Imagine that you are an assistant in a government of a small town. The town has
1. Parks
2. Streets

Since the town is small, it only has 3 parks and 4 streets. But more can be
added in the future.
Both parks and streets have a name and a year when they were built.

You are asked to generate a report containing various info regarding parks and
streets.

Generate report, print it to console.
1) Tree density for each park (# of trees / park area)
2) Average age of parks (sum of ages / # parks)
3) Name(s) of a park(s) that have more thAn 1k trees
4) Total and average length of the town's streets
5) Size classification of each street (tiny/small/normal/big/huge)
*/

class CityStructure {
    constructor(name, buildYear) {
        this.name = name;
        this.buildYear = buildYear;
    }
}

class Park extends CityStructure {
    constructor(name, buildYear, area, numberOfTrees) {
        super(name, buildYear);
        this.area = area;
        this.numberOfTrees = numberOfTrees;
    }

    reportDensity() {
        const density = this.numberOfTrees / this.area;
        console.log(`${this.name} has a tree density of ${density} trees per square km.`)
    }
}

class Street extends CityStructure {
    constructor(name, buildYear, length, size = 3) {
        super(name, buildYear);
        this.length = length;
        this.size = size;
    }

    reportClassification() {
        const classification = new Map();
        classification.set(1,'tiny');
        classification.set(2,'small');
        classification.set(3,'normal');
        classification.set(4,'big');
        classification.set(5,'huge');
        console.log(`${this.name}, built in ${this.buildYear}, is a ${classification.get(this.size)} street.`)
    }
}

function calcSumAndAverage(arr) {
/*
Reduce is an array method that allows us to reduce an array to a single value.
It accepts 2 arguments. Second one is the initial value that an accumulator
variable is set to (here accumulator variable is called counter). First argument
is a function that uses said accumulator to reduce the array to a value.
What basically happens is 'reduce' applys the function provided to each element
of the array and then sets accumulator value to be equal to the return value of
the function.
*/
    const sum = arr.reduce((counter, current, index) => counter + current, 0);
    return [sum, sum / arr.length];
}

function reportParks(parks) {
    console.log('-----PARKS REPORT-----');

    parks.forEach(p => p.reportDensity());

    const ages = parks.map(p => new Date().getFullYear() - p.buildYear);
    const [totalAge, avgAge] = calcSumAndAverage(ages);
    console.log(`Our ${parks.length} parks have an average age of ${avgAge} years`);
/*
Filter is a method that can be used to create an array from the initial array
with certain elements filtered out. It expects a filter function and applies it
to each element of the initial array. If the function evaluates to 'true', 'filter'
keeps the element to be included into the output array.
*/
    const parks1k = parks.filter(p => p.numberOfTrees >= 1000);
    if(parks1k.length > 0) {
        console.log('Parks with more than 1000 trees:');
        parks1k.forEach(p => console.log(`${p.name}: ${p.numberOfTrees} trees`));
    }
}

function reportStreets(streets) {
    console.log('-----STREETS REPORT-----');
    const [totalLength, avgLength] = calcSumAndAverage(streets.map(s => s.length));
    console.log(`Our ${streets.length} streets have a total length of ${totalLength} km, with an average of ${avgLength} km.`);

    streets.forEach(s => s.reportClassification());
}

const allParks = [new Park('Tammsaare park', 1950, 0.5, 100),
                  new Park('Kadriorg', 1846, 5, 1500),
                  new Park('Jõe park', 1780, 1.5, 400)];

const allStreets = [new Street('Läänemere tee', 1975, 2),
                    new Street('Katarina käik', 1548, 0.4, 1),
                    new Street('Mustakivi tee', 1979, 4, 4)];

reportParks(allParks);
reportStreets(allStreets);