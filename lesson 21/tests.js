QUnit.module('Controller', (hooks) => {
    let controller, mockGame = {
        submittedScore: 0,
        rollCalled: false,
        roll: function (score) {
            this.submittedScore = score;
            this.rollCalled = true;
        }
    };

    hooks.beforeEach(() => {
        mockGame.submittedScore = 0;
        controller = new Controller(mockGame);
        document.getElementById('qunit-fixture').innerHTML = '<input type="number" id="roll-score"><input type="button" id="submit-score" value="Submit">';
        controller.init();
    });

    QUnit.test('score cleared when "Submit" pressed', (assert) => {
        document.getElementById('roll-score').value = 3;
        clickButtonById('submit-score');

        assert.equal(document.getElementById('roll-score').value, '');
    });

    QUnit.test('score submitted to game', (assert) => {
        document.getElementById('roll-score').value = 3;
        clickButtonById('submit-score');

        assert.equal(mockGame.submittedScore, '3')
    });

    QUnit.test('rolls can\'t be more than 10', (assert) => {
        document.getElementById('roll-score').value = 15;
        clickButtonById('submit-score');

        assert.notOk(mockGame.rollCalled);
    });

    function clickButtonById(id) {
        const click = new Event('click');
        document.getElementById(id).dispatchEvent(click);
    }
});

QUnit.module('game', (hooks) => {
    let game;

    hooks.beforeEach(() => {
        game = new Game();
    });

    QUnit.test('failed game', (assert) => {
        rollManyOf(20, 0);

        assert.equal(game.calculateScore(), 0);
    });

    QUnit.test('all ones', (assert) => {
        rollManyOf(20,1);

        assert.equal(game.calculateScore(), 20);
    });

    QUnit.test('all spares', (assert) => {
        rollManyOf(21, 5);

        assert.equal(game.calculateScore(), 150)
    });

    QUnit.test('one strike', (assert) => {
        rollStrike();
        game.roll(3);
        game.roll(4);
        rollManyOf(16, 0);

        assert.equal(game.calculateScore(), 24)
    });

    function rollStrike() {
        game.roll(10);
    }

    function rollManyOf(balls, score) {
        for (let i = 0; i < balls; i++) {
            game.roll(score);
        }
    }
});