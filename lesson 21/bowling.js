class Controller {
    constructor(game) {
        this.game = game;
    }


    init() {
        document.getElementById('submit-score').addEventListener('click', () => {
            const scoreInput = document.getElementById('roll-score');
            if(scoreInput.value <= 10) {
                this.game.roll(scoreInput.value);
            }
            scoreInput.value = '';
        })
    }
}

class Game {
    constructor() {
        this.scores = [];
        this.currentBall = 0;
    }

    roll(rollScore) {
        this.scores[this.currentBall] = rollScore;
        this.currentBall++;
    }

    calculateScore() {
        let finalScore = 0, firstInFrame = 0;
        for (let frame = 0; frame < 10; frame++) {
            if (this.isSpare(firstInFrame)) {
                finalScore += this.addTwoBallsForFrame(firstInFrame) + this.scores[firstInFrame + 2];
                firstInFrame += 2;
            }
            else if(this.isStrike(firstInFrame)) {
                finalScore += 10 + this.scores[firstInFrame + 1] + this.scores[firstInFrame + 2];
                firstInFrame++;
            }
            else {
                finalScore += this.addTwoBallsForFrame(firstInFrame);
                firstInFrame += 2;
            }
        }
        return finalScore;
    }

    isSpare(firstInFrame) {
        return this.addTwoBallsForFrame(firstInFrame) === 10
    }

    isStrike(firstInFrame) {
        return this.scores[firstInFrame] === 10
    }

    addTwoBallsForFrame(firstInFrame) {
        return this.scores[firstInFrame] + this.scores[firstInFrame + 1]
    }
}