/*
As stated in the previous lesson, everything in js that is not a primitive,
is an object. Which means that functions are also objects and all of the
operations you could perform on an object, you could perform on a function.
Let's explore the ramifications of that.
*/

var CURRENT_YEAR = 2019;

var years = [1999, 2010, 1980, 1968];
/*
Here, we have a function that accepts another function as an argument. It's a
plain and simple way to apply a function to every element of an array, for
instance. Functions that are passed to other functions to be called by them
are usually called "callbacks".
*/
function arrayCalc(arr, fn) {
    var resultArray = [];
    for (var i = 0; i < arr.length; i++) {
        resultArray.push(fn(arr[i])); //apply function fn to the i-th element
                                      //of the array
    }
    return resultArray;
}

//below are some examples of arrayCalc being called with different functions

function calculateAge(year) {
    return CURRENT_YEAR - year;
}

console.log(arrayCalc(years, calculateAge));

function isOfLegalAge(year) {
    return calculateAge(year) >= 18;
}

console.log(arrayCalc(years, isOfLegalAge));

/*
In addition to being passed as arguments, functions can also return other
functions. The function below returns functons that generate an interview
question based on the job title provided
*/

function interviewQuestion(jobTitle) {
    if (jobTitle === 'software engineer') {
        return function (name) {
            console.log(name + ', can you tell me how well do you know java?');
        }
    }
    else if (jobTitle === 'designer') {
        return function (name) {
            console.log('Hello, ' + name + "! Do you know what UX design is?");
        }
    }
    else {
        return function (name) {
            console.log('Hi,' + name + '! What do you do?');
        }
    }
}

/*
Here is a demonstration of a function being generated, saved in a variable
and called with an argument.
*/

var seQuestion = interviewQuestion('software engineer');
seQuestion('James');

/*
You don't have to save a function to call it. Just placing braces () after any
expression that evaluates to a function is enough to call it.
*/

interviewQuestion('designer')('John');

//Immediately Invoked Function Expressions (IIFE)
/*
The property demonstrated above is used to create IIFEs. These are very useful
in any project that has more then one javascript file in it. Generally you don't
want to pollute the global scope with different variables and want to keep them
localized to the chunks of the code they are used in. IIFEs are used to do just
that
*/

var modifier = 5;

/*
Below is an example of a very simple IIFE. The first set of braces tells js
that this is not a function declaration, but in fact a statement. In it, we
define an anonymous function, which we then call using the last set of braces.
This example also demonstrates how you can provide parameters to your IIFEs.
*/

(function (m) {
    var score = Math.random() * 20;
    console.log(score + m >= 10);
})(modifier);

// console.log(score); - won't work. 'score' is local to the anonymous function

/*

/*
Let's continue our tabletop RPG theme.
As I explained on the lecture, whenever a player in an RPG declares an action
that has a chance of failing, they have to roll a dice, add a modifier from
their character attributes and checks the result against a target number to
see if they succeed. 20 sided die shown in the lecture is not the only unusual
die used in such games. There are 4, 10, 12 and even 100 sided dice available.

Complete the code below so that you could emulate that process by generating
an n-sided die and performing some skill checks that may or may not fail
depending on your luck.
Also, make sure to keep the global scope clean of any of your functions.
*/

function getDie(n) {
    return function() {
        return Math.floor(Math.random() * n) + 1;
    }
}

function isSkillCheckSuccessful(die, modifier, target) {

}